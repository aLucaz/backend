from application.api import api
from application.api import manager

if __name__ == "__main__":
    '''
        Para correr: 
        comentar manager.run()

    '''
    api.app.run(debug=True) 
    '''
        Para migrar: 
        comentar api.app.run(debug=True)
        descomentar manager.run()
        
        correr en terminal
        python run.py db init
        python run.py db migrate
        python run.py db upgrade
    '''
    #manager.run()
