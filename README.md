# Sistema de gestion de eventos academicos

Proyecto académico Ingeniería de Software

### Prerequisites

- Python 3.*  

### Installing

Para correr el Backend realizar los siguientes pasos

1. Instalar todas las dependencias en un virtualenv:

```
$ pip install -U -r requirements.txt
```

2. Correr run.py 

```
$ python run.py
```
