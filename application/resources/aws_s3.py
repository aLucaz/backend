import os 
import boto3
import botocore
from flask import session

# CREDENCIALES AWS
S3_BUCKET = os.environ.get("S3_BUCKET")
S3_KEY = os.environ.get("S3_KEY")
S3_SECRET_ACCESS_KEY = os.environ.get("S3_SECRET_ACCESS_KEY")
S3_URL = 'http://{}.s3.amazonaws.com'.format(S3_BUCKET)

# AWS S3 SERVICES 

s3_resource = boto3.resource(
            's3',
            aws_access_key_id=S3_KEY,
            aws_secret_access_key=S3_SECRET_ACCESS_KEY
        )

def upload_file_to_s3(file_content, file_name, acl="public-read"): 
    if S3_KEY and S3_SECRET_ACCESS_KEY : 
        try: 
            s3_resource.Bucket(S3_BUCKET).put_object(
                Key=file_name, Body=file_content, ACL='public-read', ContentType='application/pdf')

        except botocore.exceptions.ClientError as e: 
            raise
        return "{}/{}".format(S3_URL, file_name)

def upload_image_to_s3(file_content, file_name, acl="public-read"): 
    if S3_KEY and S3_SECRET_ACCESS_KEY : 
        try: 
            s3_resource.Bucket(S3_BUCKET).put_object(
                Key=file_name, Body=file_content, ACL='public-read', ContentType='image/jpeg')

        except botocore.exceptions.ClientError as e: 
            raise
        return "{}/{}".format(S3_URL, file_name)

def delete_file_of_s3(file_name):
    if S3_KEY and S3_SECRET_ACCESS_KEY:
        try:
            file_name = file_name[len(S3_URL)+1:]
            return s3_resource.Bucket(S3_BUCKET).delete_objects(Delete={'Objects': [{'Key': file_name}]})
        except botocore.exceptions.ClientError as e:
            raise
        return "gaa"
