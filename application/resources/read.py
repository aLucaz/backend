import xml.etree.ElementTree as ET
import os

here = os.path.dirname(os.path.abspath(__file__))
path = os.path.join(here, 'conection.xml')

tree = ET.parse(path)
root = tree.getroot()

# Parameters
user = root[0][0].text
password = root[0][1].text
host = root[0][2].text
port = root[0][3].text
dbname = root[0][4].text
