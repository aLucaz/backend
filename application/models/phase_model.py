from application.api import db

class PhaseModel(db.Model): 
    __tablename__ = 'phase' 

    id = db.Column(db.Integer, primary_key = True)
    name = db.Column(db.String(120), nullable = False)
    status = db.Column(db.Integer, nullable=True)
    description = db.Column(db.String(500), nullable = False)
    start_date = db.Column(db.DateTime, nullable = False)
    end_date = db.Column(db.DateTime, nullable = False)
    needs_document = db.Column(db.SmallInteger) 
    is_cam_rdy = db.Column(db.SmallInteger)
    event_id = db.Column(db.Integer, db.ForeignKey('event.id'))
    criteria = db.relationship('CriteriumModel', backref='phase')
    fields = db.relationship('FieldModel', backref='phase')
    submissions = db.relationship('SubmissionModel', backref='phase')
    limit_date = db.Column(db.DateTime, nullable = True)
    #Trazabilidad
    creation_date = db.Column(db.DateTime, nullable = True)
    last_modification_date = db.Column(db.DateTime, nullable = True)
    creation_user_id = db.Column(db.Integer, nullable = True)
    last_modified_user_id = db.Column(db.Integer, nullable = True)
