from application.api import db

user_permission = db.Table('user_permission',
    db.Column('user_id', db.Integer, db.ForeignKey('user.id'), primary_key=True),
    db.Column('permission_id',db.Integer, db.ForeignKey('permission.id'), primary_key=True)
)

class PermissionModel(db.Model): 
    __tablename__ = 'permission' 

    id = db.Column(db.Integer, primary_key = True)
    name = db.Column(db.String(120), nullable = False)
    users = db.relationship('UserModel', secondary=user_permission, backref=db.backref(
        'user_permission', lazy='dynamic'))

    def __repr__(self):
        return "<Permiso id='%s'>" % (self.id)
