from application.api import db

class 	FieldModel(db.Model): 
    __tablename__ = 'field' 

    id = db.Column(db.Integer, primary_key = True)
    name = db.Column(db.String(50), nullable = False)
    content = db.Column(db.String(500), nullable = True)
    phase_id = db.Column(db.Integer, db.ForeignKey('phase.id'))
    #Trazabilidad
    creation_date = db.Column(db.DateTime, nullable = True)
    last_modification_date = db.Column(db.DateTime, nullable = True)
    creation_user_id = db.Column(db.Integer, nullable = True)
    last_modified_user_id = db.Column(db.Integer, nullable = True)