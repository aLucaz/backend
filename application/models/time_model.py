from application.api import db

class TimeModel(db.Model):
    __tablename__ = 'time'

    id = db.Column(db.Integer, primary_key=True)
    time = db.Column(db.DateTime, nullable=False)

    def __repr__(self):
        return "<Time id='%s'>" % (self.id)
