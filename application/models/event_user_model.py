from application.api import db

event_user_keyword = db.Table('event_user_keyword',
    db.Column('event_user_id', db.Integer, db.ForeignKey('event_user.id'), primary_key=True),
    db.Column('keyword_id', db.Integer, db.ForeignKey('keyword.id'), primary_key=True)
)
class EventUserModel(db.Model): 
    __tablename__="event_user"
    __table_args__ = (db.UniqueConstraint('event_id', 'user_id',name='unique_component_commit'),)
    id = db.Column(db.Integer, primary_key = True)
    event_id = db.Column(db.Integer, db.ForeignKey('event.id'))
    user_id = db.Column(db.Integer, db.ForeignKey('user.id'))
    #Los keyword en este caso vendrían a ser preferencias de evaluadores
    keywords = db.relationship('KeywordModel', secondary=event_user_keyword, backref=db.backref('event_user_keyword',lazy='dynamic'))
    status = db.Column(db.Integer, default=1)

    def __repr__(self): 
        return "<EventoUser id='%s'>" % (self.id)
