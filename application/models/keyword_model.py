from application.api import db


class KeywordModel(db.Model): 
    __tablename__ = 'keyword' 

    id = db.Column(db.Integer, primary_key = True)
    name = db.Column(db.String(120), nullable = False)
    #Trazabilidad
    creation_date = db.Column(db.DateTime, nullable = True)
    last_modification_date = db.Column(db.DateTime, nullable = True)
    creation_user_id = db.Column(db.Integer, nullable = True)
    last_modified_user_id = db.Column(db.Integer, nullable = True)

    def __repr__(self):
        return "<Keyword id='%s'>" % (self.id)
