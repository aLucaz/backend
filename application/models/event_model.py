from application.api import db

event_keyword = db.Table('event_keyword',
    db.Column('event_id', db.Integer, db.ForeignKey('event.id'), primary_key=True),
    db.Column('keyword_id', db.Integer, db.ForeignKey('keyword.id'), primary_key=True)
)

class EventModel(db.Model): 
    __tablename__ = 'event' 

    id = db.Column(db.Integer, primary_key = True)
    status = db.Column(db.Integer, default=1)
    amount = db.Column(db.Float, nullable = True)
    name = db.Column(db.String(120), nullable=False)
    description = db.Column(db.String(500), nullable = False)
    vacancies = db.Column(db.Integer, nullable =False) 
    remaining_vacancies = db.Column(db.Integer, nullable =False) 
    location = db.Column(db.String(200), nullable = False)
    #si usamos aws esto debe cambiar a String
    programme = db.Column(db.String(200), nullable =True) # pdf del programa del evento 
    picture = db.Column(db.String(200), nullable=True) # imagen del evento
    start_date = db.Column(db.DateTime, nullable =True)
    end_date = db.Column(db.DateTime, nullable =True)
    limit_date_call_for_papers = db.Column(db.DateTime, nullable =True)
    type_id = db.Column(db.Integer, db.ForeignKey('type.id'))
    keywords = db.relationship('KeywordModel', secondary=event_keyword, backref=db.backref('event_keyword',lazy='dynamic'))
    users = db.relationship("EventUserModel", backref='event')
    phases = db.relationship('PhaseModel', backref='event')
    #Trazabilidad
    creation_date = db.Column(db.DateTime, nullable = True)
    last_modification_date = db.Column(db.DateTime, nullable = True)
    creation_user_id = db.Column(db.Integer, nullable = True)
    last_modified_user_id = db.Column(db.Integer, nullable = True)
    
    def __repr__(self): 
        return "<Evento id='%s'>" % (self.id)
