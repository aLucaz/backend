from application.api import db
#from passlib.hash import pbkdf2_sha256 as sha256
from passlib.handlers.pbkdf2 import pbkdf2_sha256 as sha256 

class UserModel(db.Model):
    __tablename__ = 'user'

    id = db.Column(db.Integer, primary_key=True)
    username = db.Column(db.String(120), unique=True, nullable=False)
    password = db.Column(db.String(120), nullable=False)
    request_organizer = db.Column(db.Integer, nullable=True, default=0)#0 no pidio permiso, 1 pidio permiso
    fk_person_id = db.Column(db.Integer, db.ForeignKey('person.id'))
    events = db.relationship('EventUserModel', backref='user')
    submissions = db.relationship('SubmissionModel', backref='user')
    evaluations = db.relationship('EvaluationModel', backref='user')
    #Trazabilidad
    creation_date = db.Column(db.DateTime, nullable = True)
    last_modification_date = db.Column(db.DateTime, nullable = True)
    creation_user_id = db.Column(db.Integer, nullable = True)
    last_modified_user_id = db.Column(db.Integer, nullable = True)
   
    @staticmethod
    def generate_hash(password):
        return sha256.hash(password)

    @staticmethod
    def verify_hash(password, hash):
        return sha256.verify(password, hash)

    def __repr__(self):
        return "<User id='%s'>" % (self.id)
