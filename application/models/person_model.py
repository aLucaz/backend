from application.api import db
#from sqlalchemy.orm import backref


class PersonModel(db.Model):
    __tablename__ = 'person'

    id = db.Column(db.Integer, primary_key=True)
    first_name = db.Column(db.String(120), nullable=False)
    last_name = db.Column(db.String(120), nullable=False)
    email = db.Column(db.String(120), nullable=False)
    user = db.relationship('UserModel', backref='person', uselist=False)
    #Solo para el autor
    affiliation = db.Column(db.String(120), nullable=True)
    #Trazabilidad
    creation_date = db.Column(db.DateTime, nullable = True)
    last_modification_date = db.Column(db.DateTime, nullable = True)
    creation_user_id = db.Column(db.Integer, nullable = True)
    last_modified_user_id = db.Column(db.Integer, nullable = True)

    def __repr__(self):
        return "<Persona id='%s'>" % (self.id)
