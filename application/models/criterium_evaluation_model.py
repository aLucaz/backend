from application.api import db

class CriteriumEvaluationModel(db.Model):
    __tablename__ = 'criterium_evaluation'

    id = db.Column(db.Integer, primary_key=True)
    criterium_id = db.Column(db.Integer, db.ForeignKey('criterium.id'))
    evaluation_id = db.Column(db.Integer, db.ForeignKey('evaluation.id'))
    score = db.Column(db.Integer, nullable = False)
    #Trazabilidad
    creation_date = db.Column(db.DateTime, nullable = True)
    last_modification_date = db.Column(db.DateTime, nullable = True)
    creation_user_id = db.Column(db.Integer, nullable = True)
    last_modified_user_id = db.Column(db.Integer, nullable = True)
