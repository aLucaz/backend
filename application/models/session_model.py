from application.api import db

class SessionModel(db.Model):
    __tablename__ = 'session'

    id = db.Column(db.Integer, primary_key=True)
    date_session = db.Column(db.DateTime, nullable=True)
