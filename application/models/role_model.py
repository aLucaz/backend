from application.api import db

event_user_role = db.Table('event_user_role',
    db.Column('event_user_id',db.Integer , db.ForeignKey('event_user.id'), primary_key=True),
    db.Column('role_id',db.Integer , db.ForeignKey('role.id'), primary_key=True),
)

class RoleModel(db.Model): 
    __tablename__ = 'role' 

    id = db.Column(db.Integer, primary_key = True)
    name = db.Column(db.String(120), unique= True, nullable = False)
    event_user_roles = db.relationship('EventUserModel', secondary=event_user_role, backref=db.backref(
        'event_user_role', lazy='dynamic'))

    def __repr__(self):
        return "<Rol id='%s'>" % (self.id)
