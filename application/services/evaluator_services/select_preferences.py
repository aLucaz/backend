from flask_restful import Resource, reqparse
from application.models.user_model import UserModel
from application.models.preference_model import PreferenceModel
from flask import jsonify, json

from application.controller.person_controller import PersonController
from application.controller.user_controller import UserController
from application.controller.submission_controller import SubmissionController
from application.controller.preference_controller import PreferenceController

from application.resources.write_to_log import write_to_log
from datetime import datetime

parser = reqparse.RequestParser()
parser.add_argument('username', help='Not Blank', required=True)
parser.add_argument('preferences', help='Not Blank', required=True)

class InsertPreferences(Resource): 
      def post(self):
        data = parser.parse_args()
        now = datetime.now()

        if '@' in str(data['username']):
            current_person = PersonController.query_person_by_email(
                data['username'])
            if not current_person:
                return jsonify(status="error", message="Person with email {} not exist".format(data['username']))
            current_user = UserController.query_user_by_person_id(
                current_person.id)
        else:
            current_user = UserController.query_user_by_username(
                data['username'])
            if not current_user:
                return jsonify(status="error", message='Username {} not exist'.format(data['username']))

        for preference in json.loads(data['preferences']):
            submission = SubmissionController.query_by_id(preference['submission_id'])
            if not submission: 
                return jsonify(status="error", message='Submission does not exist')
            d = preference['level']
            if d == 'Si me interesa':
                l = 1
            elif d == 'Quizás me interesa':
                l = 2
            elif d == 'No me interesa':
                l = 3
            elif d == 'Conflicto de interés':
                l = 4
            new_preference = PreferenceModel(
                    user_id=current_user.id,
                    submission_id=preference['submission_id'],
                    level=l,
                    creation_date=now,
                    creation_user_id=current_user.id
            )

            PreferenceController.insert_preference(new_preference)
            write_to_log(now.strftime("%d-%m-%Y (%H:%M:%S): ") + "El evaluador " + data['username'] + " registró en el entregable " + str(
                preference['submission_id']) + " la preferencia \"" + str(d) + "\"\n", 'log.txt')
        
        return jsonify(status="ok", message='Preference assigned')
