from flask_restful import Resource, reqparse
from application.controller.person_controller import PersonController
from application.controller.user_controller import UserController
from application.controller.event_user_controller import EventUserController
from application.controller.event_controller import EventController
from application.controller.event_user_role_controller import EventUserRoleController
from application.controller.submission_controller import SubmissionController

from flask import jsonify, json

parser = reqparse.RequestParser()
parser.add_argument('name_submission', help='Not Blank', required=True)

class SendSubmission(Resource):  
    def post(self):
        data = parser.parse_args()
        name_submission = data['name_submission']
        submission = SubmissionController.query_submission_by_name(name_submission)        
        if not submission: 
            return jsonify(status="error", message="No se encontró ese entregable")
        return jsonify(status="ok", document=submission.document)
