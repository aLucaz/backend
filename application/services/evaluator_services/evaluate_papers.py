from flask_restful import Resource, reqparse
from application.models.criterium_model import CriteriumModel
from application.models.criterium_evaluation_model import CriteriumEvaluationModel

from flask import jsonify, request

from application.controller.criterium_controller import CriteriumController
from application.controller.evaluation_controller import EvaluationController
from application.controller.criterium_evaluation_controller import CriteriumEvaluationController
from application.controller.evaluation_controller import EvaluationController
from application.controller.user_controller import UserController

from application.resources.write_to_log import write_to_log

import json

from datetime import datetime

parser = reqparse.RequestParser()
parser.add_argument('username', help='Not Blank', required=True)
parser.add_argument('id', help='Not Blank', required=True)
parser.add_argument('trust', help='Not Blank', required=True)
parser.add_argument('score', help='Not Blank', required=True)
parser.add_argument('criterium_evaluation', help='Not Blank', required=True)
parser.add_argument('comment_author', help='Not Blank', required=False)
parser.add_argument('comment_president', help='Not Blank', required=False)

class UpdateEvaluation(Resource):
    def post(self):
        data = parser.parse_args()
        now = datetime.now()
        user = UserController.query_user_by_username(data['username'])
        user_id = user.id if user else None
        nuevo=False
        current_evaluation = EvaluationController.query_by_id(data['id'])
        evaluation_id = current_evaluation.id

        criteriums_evaluations = CriteriumEvaluationController.query_by_evaluation(evaluation_id)    
        
        for criterium in json.loads(data['criterium_evaluation']):
            score=criterium['score']
            if score == 'Muy malo':
                s = 1
            elif score == 'Malo':
                s = 2
            elif score == 'Regular':
                s = 3
            elif score == 'Bueno':
                s = 4
            elif score == 'Muy bueno':
                s = 5

            id_criterio = 0
            for evaluations in criteriums_evaluations:
                if criterium['criterium_id'] == evaluations[1].id:
                    id_criterio = criterium['criterium_id']

            if id_criterio != 0:
                new_criterium_evaluation = CriteriumEvaluationController.query_by_evaluation_by_criterium(evaluation_id,id_criterio)
                new_criterium_evaluation.score = s
                new_criterium_evaluation.last_modification_date = now
                new_criterium_evaluation.last_modified_user_id = user_id


            else:
                nuevo=True
                new_criterium_evaluation = CriteriumEvaluationModel(
                    criterium_id=criterium['criterium_id'],
                    evaluation_id=evaluation_id,
                    score=s,
                    creation_date=now,
                    creation_user_id=user_id
                )
            CriteriumEvaluationController.insert(new_criterium_evaluation)
        print("----------------")
        print(data['trust'])
        trust = data['trust']
        if trust == 'Muy malo':
            t = 1
        elif trust == 'Malo':
            t = 2
        elif trust == 'Regular':
            t = 3
        elif trust == 'Bueno':
            t = 4
        elif trust == 'Muy bueno':
            t = 5
            
        total_score = data['score']
        if total_score == 'Fuerte Rechazo':
            ts = 1
        elif total_score == 'Rechazo':
            ts = 2
        elif total_score == 'Indeciso':
            ts = 3
        elif total_score == 'Débil Aceptación':
            ts = 4
        elif total_score == 'Fuerte Aceptación':
            ts = 5
            
        current_evaluation.trust = t
        current_evaluation.score = ts
        current_evaluation.comment_author = data['comment_author']
        current_evaluation.comment_president = data['comment_president']
        current_evaluation.evaluated = 1
        if not nuevo:current_evaluation.last_modification_date=now
        if not nuevo: current_evaluation.last_modified_user_id=user_id
        
        EvaluationController.insert_evaluation(current_evaluation)        
        write_to_log(now.strftime("%d-%m-%Y (%H:%M:%S): ") + "Se actualizó la evaluación " + data['id'] + " \n", 'log.txt')

        return jsonify(status="ok", message="evaluation successful")
