from flask_restful import Resource, reqparse
from application.controller.person_controller import PersonController
from application.controller.event_user_controller import EventUserController
from application.controller.event_controller import EventController
from application.controller.event_user_role_controller import EventUserRoleController
from application.controller.phase_controller import PhaseController
from application.controller.submission_controller import SubmissionController
from application.controller.keyword_controller import KeywordController
from application.controller.criterium_evaluation_controller import CriteriumEvaluationController
from application.controller.criterium_controller import CriteriumController
from application.controller.evaluation_controller import EvaluationController

from flask import jsonify, json

parser = reqparse.RequestParser()
parser.add_argument('id', help='Not Blank', required=True)

class SendEvaluationData(Resource):
    def post(self): 
        data = parser.parse_args()
        current_evaluation = EvaluationController.query_by_id(data['id'])
        if not current_evaluation:
            return jsonify(status="error", message="Evaluation doesn't exist")

        submission = SubmissionController.query_by_id(current_evaluation.submission_id)
        if not submission:
            return jsonify(status="error", message="Submission doesn't exist")

        phase = PhaseController.query_phase_by_id(submission.phase_id)
        if not phase:
            return jsonify(status="error", message="Phase doesn't exist")

        criteria = CriteriumController.query_by_phase_id(phase.id)

        keywords = KeywordController.query_keywords_by_submission(submission.id)

        authors = PersonController.query_by_submission(submission.id)

        keywords_json = []
        for keyword in keywords:
            keywords_json.append(keyword.name)

        criteria_json = []
        for criterium in criteria:
            criterium_evaluation = CriteriumEvaluationController.query_by_evaluation_by_criterium(current_evaluation.id,criterium.id)
            criteria_json.append({
                "id": criterium.id,
                "name": criterium.name,
                "score": criterium_evaluation.score})

        authors_json = []
        for author in authors:
            person = PersonController.query_by_id(author.person_id)
            authors_json.append({
                "id": person.id,
                "first_name": person.first_name,
                "last_name": person.last_name})

        info = {
                "name":submission.name,
                "keyword":keywords_json,
                "summary":submission.summary,
                "authors":authors_json,
                "criteria":criteria_json,
                "comment_author":current_evaluation.comment_author,
                "comment_president":current_evaluation.comment_president,
                "trust":current_evaluation.trust,
                "score":current_evaluation.score
                }
        return jsonify(info=info)