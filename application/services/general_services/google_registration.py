from flask_restful import Resource, reqparse
from application.models.person_model import PersonModel
from application.models.user_model import UserModel

from application.controller.user_controller import UserController
from application.controller.person_controller import PersonController

from flask import jsonify
from application.resources.write_to_log import write_to_log
from google.oauth2 import id_token
from google.auth.transport import requests
import os 
from datetime import datetime

GOOGLE_CLIENT_ID = os.environ.get("GOOGLE_CLIENT_ID")

parser = reqparse.RequestParser()
parser.add_argument('first_name', help='Not Blank', required=True)
parser.add_argument('last_name', help='Not Blank', required=True)
parser.add_argument('email', help='Not Blank', required=True)
parser.add_argument('username', help='Not Blank', required=True)
parser.add_argument('token', help='Not Blank', required=True)



class GoogleRegistration(Resource):
    def post(self):
        data = parser.parse_args()
        now = datetime.now()
        #validación de usuario
        if UserController.query_user_by_username(data['username']):
            return jsonify(status="error", message="User {} already exists".format(data['username']))

        #validación de persona
        if PersonController.query_person_by_email(data['email']):
            return jsonify(status="error", message="Person with email {} already exists".format(data['email']))

        new_person = PersonModel(
            first_name=data['first_name'],
            last_name=data['last_name'],
            email=data['email'],
            creation_date=now
        )

        # se procesa el token 
        try:
            idinfo = id_token.verify_oauth2_token(
                data['token'], requests.Request(), GOOGLE_CLIENT_ID)
            if idinfo['iss'] not in ['accounts.google.com', 'https://accounts.google.com']:
                print('===> Wrong issuer.')
            userid = idinfo['sub']
            print('=================>>>> mi contraseña: {}'.format(userid))
        except:
            # Invalid token
            return jsonify(status="error", message="Invalid Token")

        new_user = UserModel(
            username=data['username'],
            password=UserModel.generate_hash(userid),
            person=new_person,
            creation_date=now
        )
        PersonController.insert_person(new_person)
        UserController.insert_user(new_user)

        new_person.creation_user_id=new_user.id
        new_user.creation_user_id=new_user.id
        PersonController.insert_person(new_person)
        # Se retorna la confirmacion de la ultima tabla afectada

        write_to_log(now.strftime("d-%m-%Y (%H:%M:%S): ") +
                     "Se registró al usuario (con cuenta de Google) " + data['username'] + "\n", 'log.txt')

        return UserController.insert_user(new_user)
