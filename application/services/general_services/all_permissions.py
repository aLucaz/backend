from flask_restful import Resource
from application.controller.permission_controller import PermissionController

class AllPermissions(Resource): 
    def post(self): 
        return PermissionController.query_all_permissions()
