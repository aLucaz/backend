from flask_restful import Resource, reqparse
from application.models.person_model import PersonModel
from application.models.user_model import UserModel

from application.controller.user_controller import UserController
from application.controller.person_controller import PersonController

from flask import jsonify
from application.resources.write_to_log import write_to_log
from datetime import datetime

parser = reqparse.RequestParser()
parser.add_argument('first_name', help='Not Blank', required=True)
parser.add_argument('last_name', help='Not Blank', required=True)
parser.add_argument('email', help='Not Blank', required=True)
parser.add_argument('username', help='Not Blank', required=True)
parser.add_argument('password', help='Not Blank', required=True)


class Registration(Resource):
    def post(self):
        data = parser.parse_args()
        now = datetime.now()
        #validación de usuario
        if UserController.query_user_by_username(data['username']):
            return jsonify(status="error", message="Usuario {} ya existe".format(data['username']))

        current_person = PersonController.query_person_by_email(data['email'])
        if current_person:
            current_person.first_name = data['first_name']
            current_person.last_name = data['last_name']
            current_person.email = data['email']
            PersonController.insert_person(current_person)                                                                                                         
            new_user = UserModel(
                username=data['username'],
                password=UserModel.generate_hash(data['password']),
                fk_person_id=current_person.id,
                creation_date=now
            )
            return UserController.insert_user(new_user)

        else:
            new_person = PersonModel(
                first_name=data['first_name'],
                last_name=data['last_name'],
                email=data['email'],
                creation_date=now
            )

            new_user = UserModel(
                username=data['username'],
                password=UserModel.generate_hash(data['password']),
                person=new_person,
                creation_date=now
            )

            PersonController.insert_person(new_person)
            UserController.insert_user(new_user)

            new_person.creation_user_id=new_user.id
            new_user.creation_user_id=new_user.id

            PersonController.insert_person(new_person)

            # Se retorna la confirmacion de la ultima tabla afectada
            write_to_log(now.strftime("%d-%m-%Y (%H:%M:%S): ") +
                         "Se registró al usuario " + data['username'] + "\n", 'log.txt')

            return UserController.insert_user(new_user)
