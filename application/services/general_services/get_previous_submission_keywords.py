from flask_restful import Resource, reqparse
from application.controller.phase_controller import PhaseController
from application.controller.submission_controller import SubmissionController
from application.controller.keyword_controller import KeywordController
from application.controller.user_controller import UserController


from flask import jsonify, json

parser = reqparse.RequestParser()
parser.add_argument('id_phase', help='Not Blank', required=True)
parser.add_argument('username', help='Not Blank', required=True)

class GetPreviousSubmissionKeywords(Resource): 
    def post(self):
        data = parser.parse_args()
        current_user = UserController.query_user_by_username(data['username'])
        phase = PhaseController.query_phase_by_id(data['id_phase'])
        submission = SubmissionController.query_by_user_and_phase(current_user.id,phase.id)
        keywords_json= []
        keywords = KeywordController.query_keywords_by_submission(submission.id)
        for keyword in keywords:
            keywords_json.append(keyword.name)

        return jsonify(keywords=keywords_json)