from flask_restful import Resource, reqparse
from application.controller.person_controller import PersonController
from application.controller.user_controller import UserController
from application.controller.permission_user_controller import PermissionUserController

from flask import jsonify, json


parser = reqparse.RequestParser()
parser.add_argument('username', help='Not Blank', required=True)

class CheckOrganizer(Resource): 
    def post(self):
        data = parser.parse_args()
        if '@' in str(data['username']):
            current_person = PersonController.query_person_by_email(
                data['username'])
            if not current_person:
                return jsonify(status="error", message="Person with email {} not exist".format(data['username']))
            #se busca el usuario de la persona
            current_user = UserController.query_user_by_person_id(
                current_person.id)
        else:
            current_user = UserController.query_user_by_username(
                data['username'])
            if not current_user:
                return jsonify(status="error", message='Username {} not exist'.format(data['username']))

        permissions_user = PermissionUserController.query_permissions_by_user_id(current_user.id)

        result = False
        if permissions_user:
            for permission_user in permissions_user:
                if permission_user.name == "Organizer":
                    result = True

        return jsonify(is_organizer=result)
