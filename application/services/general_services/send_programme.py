from flask_restful import Resource, reqparse
from application.controller.person_controller import PersonController
from application.controller.user_controller import UserController
from application.controller.event_user_controller import EventUserController
from application.controller.event_controller import EventController
from application.controller.event_user_role_controller import EventUserRoleController
from application.controller.phase_controller import PhaseController

from flask import jsonify, json

parser = reqparse.RequestParser()
parser.add_argument('id_event', help='Not Blank', required=True)

class SendProgramme(Resource): 
    def post(self):
        data = parser.parse_args()
        id_event = data['id_event']
        event = EventController.query_event_by_id(id_event)        
        return{ "programme": event.programme}
