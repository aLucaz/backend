from application.controller.person_controller import PersonController
from application.controller.user_controller import UserController
from flask_restful import reqparse, Resource 
from flask import json, jsonify

parser = reqparse.RequestParser()
parser.add_argument('emails', help='Not Blank', required=True)

class VerifyExistence(Resource): 
    def post(self): 
        data = parser.parse_args()
        json_emails = json.loads(data['emails'])
        status_emails = []
        for someone in json_emails: 
            #primero se busca al email en la tabla de persona 
            person = PersonController.query_person_by_email(someone['email'])
            
            if not person: 
                status_emails.append({
                    "email": someone['email'],
                    "status": "no existe" 
                })
            
            else: 
                #buscamos al usuario debido a que hay a veces personas sin usuario
                user = UserController.query_user_by_person_id(person.id)
                if not user: 
                    status_emails.append({
                        "email": someone['email'],
                        "status": "no existe"
                    })
                else: 
                    status_emails.append({
                        "email": someone['email'],
                        "status": "existe"
                    })
        #se retorna el estado de todos los emails enviados                     
        return jsonify(emails=status_emails)