from flask_restful import Resource, reqparse
from application.controller.person_controller import PersonController
from application.controller.user_controller import UserController
from application.controller.event_user_controller import EventUserController
from application.controller.event_controller import EventController
from application.controller.event_user_role_controller import EventUserRoleController
from application.controller.phase_controller import PhaseController

from application.models.person_model import PersonModel
from application.models.user_model import UserModel
from application.resources.write_to_log import write_to_log
from flask import jsonify, json
from datetime import datetime

parser = reqparse.RequestParser()
parser.add_argument('username', help='Not Blank', required=True)
parser.add_argument('password', help='Not Blank', required=True)

class UpdatePasswordUser(Resource): 
    def post(self):
        data = parser.parse_args()
        now = datetime.now()
        current_user = UserController.query_user_by_username(
                data['username'])
        current_user.password = UserModel.generate_hash(data['password'])
        current_user.last_modification_date=now
        current_user.last_modified_user_id=current_user.id
        UserController.insert_user(current_user)
        
        write_to_log(now.strftime("%d-%m-%Y (%H:%M:%S): ") + "El usuario " + data['username'] + "ha cambiado su contraseña\n", 'log.txt')

        notify = UserController.update_password(current_user.username,current_user.password)
        return jsonify(status="ok", message="Username {} updated satisfactorily".format(data['username']))
