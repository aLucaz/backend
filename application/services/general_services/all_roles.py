from flask_restful import Resource
from application.controller.role_controller import RoleController

class AllRoles(Resource): 
    def post(self): 
        return RoleController.query_all_roles()
    
        
