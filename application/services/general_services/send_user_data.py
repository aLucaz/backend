from flask_restful import Resource, reqparse
from application.controller.person_controller import PersonController
from application.controller.user_controller import UserController
from application.controller.event_user_controller import EventUserController
from application.controller.event_controller import EventController
from application.controller.event_user_role_controller import EventUserRoleController
from application.controller.phase_controller import PhaseController

from flask import jsonify, json

parser = reqparse.RequestParser()
parser.add_argument('username', help='Not Blank', required=True)

class SendUserData(Resource): 
    def post(self):
        data = parser.parse_args()
        current_user = UserController.query_user_by_username(
                data['username'])
        current_person = PersonController.query_by_id(current_user.fk_person_id)
        if not current_user:
            return jsonify(status="error", message='Username {} not exist'.format(data['username']))
        return{
            "username":current_user.username,
            "first_name":current_person.first_name,
            "last_name":current_person.last_name,
            "email":current_person.email
        }
