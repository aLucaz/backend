from flask_restful import Resource, reqparse
from application.models.type_model import TypeModel
from application.models.keyword_model import KeywordModel
from application.models.event_model import EventModel
from application.models.event_user_model import EventUserModel
from application.models.phase_model import PhaseModel
from application.models.criterium_model import CriteriumModel
from application.models.field_model import FieldModel

from flask import jsonify, request
from application.resources.write_to_log import write_to_log
from application.controller.event_controller import EventController
from application.controller.event_user_controller import EventUserController
from application.controller.type_controller import TypeController
from application.controller.keyword_controller import KeywordController
from application.controller.user_controller import UserController
from application.controller.person_controller import PersonController
from application.controller.role_controller import RoleController
from application.controller.phase_controller import PhaseController
from application.controller.field_controller import FieldController
from application.controller.criterium_controller import CriteriumController

from application.resources.aws_s3 import upload_file_to_s3, upload_image_to_s3
from application.resources.time_resources import clean_hour, parse_str_to_date

from werkzeug.datastructures import FileStorage
from werkzeug.utils import secure_filename

import json
from datetime import datetime

# HEIL FILESYSTEM
import os
from pathlib import Path
from datetime import datetime

parser = reqparse.RequestParser()
parser.add_argument('name', help='Not Blank', required=True)
parser.add_argument('description', help='Not Blank', required=True)
parser.add_argument('vacancies', help='Not Blank', required=True)
parser.add_argument('location', help='Not Blank', required=True)
parser.add_argument('start_date', help='Not Blank', required=False)
parser.add_argument('end_date', help='Not Blank', required=False)
parser.add_argument('keywords', help='Not Blank', required=True)
parser.add_argument('type', help='Not Blank', required=True)
parser.add_argument('members', help='Not Blank', required=True)
parser.add_argument('programme', type=FileStorage, help='Not Blank', required=False, location='files')
parser.add_argument('picture', type=FileStorage, help='Not Blank', required=False, location='files')
parser.add_argument('phases', help='Not Blank', required=True)

parser.add_argument('amount', required=False)
parser.add_argument('status', required=False)
parser.add_argument('dayCfP', required=False)
parser.add_argument('username', help='Not Blank', required=False)

ALLOWED_EXTENSIONS = ['pdf', 'jpg', 'png', 'jpeg', 'txt']

def allowed_file(filename):
    return '.' in filename and filename.rsplit('.', 1)[1].lower() in ALLOWED_EXTENSIONS


class CreateEvent(Resource):
    def post(self):
        data = parser.parse_args()
        # se busca el type dentro de la base de datos
        current_type = TypeController.query_type_by_name(data['type'])
        now = datetime.now()
        user = UserController.query_user_by_username(data['username'])
        user_id =user.id
        # validacion de evento
        if EventController.query_event_by_name(data['name']):
            return jsonify(message="Event {} already exists".format(data['name']))

        if data['picture']: 
            picture = data['picture']
            if picture and allowed_file(picture.filename):
                picture_filename = secure_filename(picture.filename)
                picture_url = upload_image_to_s3(picture, str(picture_filename))
            else:
                return jsonify(status="error", message="No se subió la foto")
        else: 
            picture_url = " " 
            
        if data['programme']: 
            # una vez validad la data se guarda en los path correspondientes
            programme = data['programme']
            if programme and allowed_file(programme.filename):
                programme_filename = secure_filename(programme.filename)
                programme_url = upload_file_to_s3(programme, programme_filename)
            else:
                return jsonify(status="error", message="No se subió el paper")
        else: 
            programme_url= " "
        

        # una vez que ya se tiene toda la data se crea la instancia de evento
        new_event = EventModel(
            name=data['name'],
            description=data['description'],
            vacancies=data['vacancies'],
            remaining_vacancies=data['vacancies'],
            location=data['location'], 
            start_date=parse_str_to_date(data['start_date']) if data['start_date'] else None,
            end_date=parse_str_to_date(data['end_date']) if data['end_date'] else None,
            type=current_type,
            programme= programme_url,
            picture= picture_url,
            amount=data['amount'],
            status=data['status'],
            limit_date_call_for_papers=parse_str_to_date(data['dayCfP']) if data['dayCfP'] else None,
            creation_date=now,
            creation_user_id=user_id,
        )

        # se busca cada keyword en la base de datos, si no lo encuentra lo agregamos
        json_keywords_data = json.loads(data['keywords'])
        for keyword in json_keywords_data:
            new_keyword = KeywordController.query_keyword_by_name(
                keyword['name'])
            if not new_keyword:
                new_keyword = KeywordModel(name=keyword['name'],creation_date=now,creation_user_id=user_id)
                notify = KeywordController.insert_keyword(new_keyword)
                if notify.status == "error":
                    return jsonify(status="error", message="Something wrong in Keyword Registration")
            # se agrega en la tabla intermedia
            new_keyword.event_keyword.append(new_event)

        #por cada fase
        json_phases_data = json.loads(data['phases'])
        for phase in json_phases_data:
            #se crea la nueva fase
            new_phase = PhaseModel(
                name=phase['name'],
                status= int(phase['status']),
                description=phase['description'],
                start_date=parse_str_to_date(phase['start_date']) if phase['start_date'] else None,
                end_date=parse_str_to_date(phase['end_date']) if phase['end_date'] else None,
                needs_document=int(phase['needs_document']),
                event=new_event,
                limit_date=parse_str_to_date(phase['limit_date']) if phase['limit_date'] else None,
                is_cam_rdy=phase['iscamrdy'],
                creation_date=now,
                creation_user_id=user_id
            )

            print(new_phase.name)

            notify = PhaseController.insert_phase(new_phase)
            if notify.status == "error":
                return jsonify(status="error", message="Something wrong in Phase Registration")

            #por cada field de cada fase
            for field in phase['fields']:
                new_field = FieldModel(
                    name=field['name'],
                    phase=new_phase,
                    creation_date=now,
                    creation_user_id=user_id
                )
                notify = FieldController.insert_field(new_field)
                if notify.status == "error":
                    return jsonify(status="error", message="Something wrong in Field Registration")

            #por cada criterium de cada fase
            for criterium in phase['criteriums']:
                new_criterium = CriteriumModel(
                    name=criterium['name'],
                    phase=new_phase,
                    creation_date=now,
                    creation_user_id=user_id
                )
                notify = CriteriumController.insert_criterium(new_criterium)
                if notify.status == "error":
                    return jsonify(status="error", message="Something wrong in Criterium Registration")


        # ESTO ESTA ACA PARA PARCHAR EL ERROR DEL DOBLE REGISTRO, LA INSERCION DOBLE DEL EVENTO , DEBE SER AL FINAL
        print("llega")
        notify = EventController.insert_event(new_event)
        print(new_event.id)
        if notify.status == "error":
            return jsonify(status="error", message="Something wrong in Event Registration")
            
        new_event_id = new_event.id
        # por cada miembro
        json_members_data = json.loads(data['members'])
        for member in json_members_data:
            # primero se busca a la persona relacionada con el e-mail
            person_aux = PersonController.query_person_by_email(member['email'])
            if person_aux: 
                #si se encuentra 
                user_aux = UserController.query_user_by_person_id(person_aux.id)
                #solo en caso de que exista el usuario, lo agrego como miembro
                if user_aux:
                    #primero se verifica que no este ya creada la relacion event x user
                    new_event_user = EventUserController.query_by_event_id_and_user_id(
                        event_id=new_event_id, user_id=user_aux.id)
                    
                    #si no está se crea
                    if not new_event_user:
                        new_event_user = EventUserModel(
                            user=user_aux,
                            event=new_event
                        )

                    #se busca el rol especificado ESTA COMO ROL EN FRONTEND NO CAMIAR!
                    role = RoleController.query_role_by_name(member['rol'])
                    #se agrega la relacion entre role y  event_user
                    new_event_user.event_user_role.append(role)
                    #finalmente se agrega a eventxuser
                    EventUserController.insert_event_user(new_event_user)

        write_to_log(now.strftime("%d-%m-%Y (%H:%M:%S): ") + "El usuario " +
                     data['username'] + " registró el evento \"" + data['name'] + "\"\n", 'log.txt')

        #esto no funcionara hasta que se mergee con gabdev
        return jsonify(status="ok", message="Event {} created satisfactorily".format(data['name']))
