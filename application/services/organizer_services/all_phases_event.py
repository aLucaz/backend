from flask_restful import Resource, reqparse
from application.controller.person_controller import PersonController
from application.controller.user_controller import UserController
from application.controller.event_user_controller import EventUserController
from application.controller.event_controller import EventController
from application.controller.event_user_role_controller import EventUserRoleController
from application.controller.phase_controller import PhaseController
from application.controller.submission_controller import SubmissionController
from datetime import datetime
from flask import jsonify, json


parser = reqparse.RequestParser()
parser.add_argument('username', help='Not Blank', required=True)
parser.add_argument('id_event', help='Not Blank', required=True)

class AllPhasesEvent(Resource): 
    def post(self):
        data = parser.parse_args()
        current_user = UserController.query_user_by_username(data['username'])
        id_event = data['id_event']
        # se obtienen los eventos del usuario
        event = EventController.query_event_by_id(id_event)
        phases = PhaseController.query_all_phases(id_event)
        phase_json = []
        
        for phase in phases:
            status = 0
            submission = SubmissionController.query_by_user_and_phase(current_user.id,phase.id)
            if submission is None:
                status = 0 
            else:
                status = submission.status
            print(status)
            # Se almacena en un json
            print("estado de la fase " + phase.name + " -> " + str(phase.status))
            phase_json.append({
                "id_phase":phase.id,
                "name_phase":phase.name,
                "status_phase":phase.status,
                "startDate":phase.start_date.strftime("%d-%m-%Y"),
                "endDate": phase.end_date.strftime("%d-%m-%Y"),
                "needs_document":phase.needs_document,
                "status_submission":status
            })
        return {
            "phases":phase_json,
            "name_event":event.name
		}
