from flask_restful import Resource, reqparse
from flask import jsonify, request
import json
from datetime import datetime
from application.models.phase_model import PhaseModel
from application.models.criterium_model import CriteriumModel
from application.models.field_model import FieldModel
from application.controller.phase_controller import PhaseController	
from application.controller.field_controller import FieldController
from application.controller.criterium_controller import CriteriumController
from application.controller.event_controller import EventController
from application.controller.user_controller import UserController

from datetime import datetime

parser = reqparse.RequestParser()
parser.add_argument('username', help='Not Blank', required=True)
parser.add_argument('event_id', help='Not Blank', required=True)
parser.add_argument('phases', help='Not Blank', required=True)

def process_date(date):
	try: 
		int(date[0])
		return datetime.strptime((date.replace("T", " "))[0:19], '%Y-%m-%d %H:%M:%S')
	except ValueError:
		#return datetime.strptime(date[0:25], '%a, %d %b %Y %H:%M:%S')
		return datetime.strptime(date[0:25], '%Y-%m-%d')


def update_phase(phase_id,phase_json,now,user_id):
	phase = PhaseController.query_phase_by_id(phase_id)
	if phase_json['name']: phase.name = phase_json['name']
	if phase_json['description']: phase.description = phase_json['description']
	if phase_json['start_date']: phase.start_date = process_date(phase_json['start_date'])
	if phase_json['end_date']: phase.end_date = process_date(phase_json['end_date'])
	if phase_json['limit_date']: phase.limit_date = process_date(phase_json['limit_date'])
	if phase_json['needs_document']: phase.needs_document = phase_json['needs_document']
	if phase_json['iscamrdy']: phase.is_cam_rdy = phase_json['iscamrdy']
	if phase_json['status']: phase.status = phase_json['status']
	print(phase_json['name'])
	print (phase_json['needs_document'])
	phase.last_modification_date=now
	phase.last_modified_user_id=user_id

	PhaseController.insert_phase(phase)
	
	#update fields asociados
	fields = FieldController.query_fields_by_id(phase_id)
	delete = [{"name":f.name,"field_model":f} for f in fields]
	new = []
	for field in phase_json['fields']:
		encontrado = False
		for f in delete:
			if field['name']==f['name']:
				delete.remove(f)
				encontrado=True
				break
		#no se encontro
		if not encontrado: new.append(field)

	for field in new:
		new_field = FieldModel(
					name=field['name'],
					phase=phase,
					creation_date=now,
					creation_user_id=user_id
				)
		notify = FieldController.insert_field(new_field)
		if notify.status == "error":
			return jsonify(status="error", message="Something wrong in Field Registration")

	for field in delete:
		FieldController.delete_field(field['field_model'])

	#update criteriums asociados
	criteriums = CriteriumController.query_by_phase_id(phase_id)
	delete = [{"name":c.name,"criterium_model":c} for c in criteriums]
	new = []
	for criterium in phase_json['criteriums']:
		encontrado = False
		for c in delete:
			if criterium['name']==c['name']:
				delete.remove(c)
				encontrado = True
				break
		#no se encontro
		if not encontrado: new.append(criterium)

	for criterium in new:
		new_criterium = CriteriumModel(
					name=criterium['name'],
					phase=phase,
					creation_date=now,
					creation_user_id=user_id
				)
		notify = CriteriumController.insert_criterium(new_criterium)
		if notify.status == "error":
			return jsonify(status="error", message="Something wrong in Criterium Registration")

	for criterium in delete:
		CriteriumController.delete_criterium(criterium['criterium_model'])


class UpdateEventPhases(Resource):
	def post(self):
		data = parser.parse_args()
		event =  EventController.query_event_by_id(data['event_id'])
		previous_phases = PhaseController.query_all_phases(data['event_id'])
		delete = [{"id":p.id,"name":p.name} for p in previous_phases]
		new = []
		now = datetime.now()
		user = UserController.query_user_by_username(data['username'])
		user_id =user.id

		#forma correcta del front
		json_phases_data = json.loads(data['phases'])
		for phase in json_phases_data:
			encontrado = False
			for p in delete:
				if not 'phase_id' in phase.keys():
					encontrado =False
					break 
				elif phase['phase_id']==p["id"]:
					update_phase(p['id'],phase,now,user_id)
					delete.remove(p)
					encontrado =True
					break
			#no lo encontró
			if not encontrado:
				new.append(phase)

		#se borran las fases que ya no se usarán
		for phase in delete:
			#primero se borran lo campos asociados
			fields = FieldController.query_fields_by_id(phase["id"])
			for field in fields:
				FieldController.delete_field(field)
			#luego se borran los criterios asociados
			criteriums = CriteriumController.query_by_phase_id(phase["id"])
			for criterium in criteriums:
				CriteriumController.delete_criterium(criterium)
			#Finalmente se borra la fase
			phase_model = PhaseController.query_phase_by_id(phase["id"])
			PhaseController.delete_phase(phase_model)


		#Se insertan las nuevas fases
		for phase in new:
			new_phase = PhaseModel(
				name=phase['name'],
				description=phase['description'],
				start_date=	process_date(phase['start_date']),
				end_date= process_date(phase['end_date']),
				event=event,
				status = phase['status'],
				limit_date=process_date(phase['limit_date']) if phase['limit_date'] else None,
				needs_document=phase['needs_document'] if phase['needs_document'] else None,
				is_cam_rdy=phase['iscamrdy'],
				creation_date=now,
				creation_user_id=user_id
			)
			
			notify = PhaseController.insert_phase(new_phase)
			if notify.status == "error":
				return jsonify(status="error", message="Something wrong in Phase Registration")


			#por cada field de cada fase
			for field in phase['fields']:
				new_field = FieldModel(
					name=field['name'],
					phase=new_phase,
					creation_date=now,
					creation_user_id=user_id
				)
				notify = FieldController.insert_field(new_field)
				if notify.status == "error":
					return jsonify(status="error", message="Something wrong in Field Registration")

			#por cada criterium de cada fase
			for criterium in phase['criteriums']:
				new_criterium = CriteriumModel(
					name=criterium['name'],
					phase=new_phase,
					creation_date=now,
					creation_user_id=user_id
				)
				notify = CriteriumController.insert_criterium(new_criterium)
				if notify.status == "error":
					return jsonify(status="error", message="Something wrong in Criterium Registration")


		return jsonify(status="ok", message="Event phases updated satisfactorily")
