from flask_restful import Resource, reqparse
from application.controller.phase_controller import PhaseController

from flask import jsonify, json
import os 
from pathlib import Path

#FILES MANAGE

parser = reqparse.RequestParser()
parser.add_argument('id', help='Not Blank', required=True)

class GetPhaseDate(Resource):  
    def post(self):
        data = parser.parse_args()
        current_phase = PhaseController.query_phase_by_id(data['id'])
        dates = {"start_date":current_phase.start_date.strftime("%d/%m/%Y"), 
                "end_date":current_phase.end_date.strftime("%d/%m/%Y")}
        return jsonify(dates=dates)
