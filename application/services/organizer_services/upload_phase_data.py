from flask_restful import Resource, reqparse
from application.controller.person_controller import PersonController
from application.controller.user_controller import UserController
from application.controller.event_user_controller import EventUserController
from application.controller.event_controller import EventController
from application.controller.event_user_role_controller import EventUserRoleController
from application.controller.phase_controller import PhaseController
from application.controller.field_controller import FieldController
from application.controller.keyword_controller import KeywordController

from flask import jsonify, json

parser = reqparse.RequestParser()
parser.add_argument('id_event', help='Not Blank', required=True)
parser.add_argument('id_phase', help='Not Blank', required=True)

class UploadPhaseData(Resource): 
    def post(self):
        data = parser.parse_args()
        id_event = data['id_event']
        id_phase = data['id_phase']
        event = EventController.query_event_by_id(id_event)
        phase = PhaseController.query_phase_by_id(id_phase)
        fields = FieldController.query_fields_by_id(id_phase)
        field_json = []
        keyword_json = []
        keywords = KeywordController.query_keywords_by_event(id_event)
        #print(keywords)
        for key in keywords:
            keyword_json.append({
                "name_category":key.name
            })
        for field in fields:
            field_json.append({
                "id_field":field.id,
                "name_field":field.name,
                "content_field":field.content
            })
        return{
            "keywords":keyword_json,
            "fields":field_json,
            "title_event":event.name,
            "startDate":phase.start_date.strftime("%d-%m-%Y"),
            "endDate":phase.end_date.strftime("%d-%m-%Y")
		}
