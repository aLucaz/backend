from flask_restful import Resource, reqparse 
from flask import jsonify, json 
from application.controller.time_controller import TimeController

class SendTime(Resource): 
    def post(self): 
        time = TimeController.query_time()
        if time: 
            return jsonify(status= "ok", time= time.time)
        else:
            return jsonify(status="error", message="time doesn't exists")