from flask_restful import reqparse, Resource
from flask import json, jsonify
from application.resources.time_resources import parse_str_to_date
from application.controller.event_controller import EventController

parser = reqparse.RequestParser()
parser.add_argument('date_start', help='Not Blank', required=True)
parser.add_argument('date_end', help='Not Blank', required=True)


class AllCreatedEvents(Resource):
    def post(self):
        data = parser.parse_args()
        date_start = parse_str_to_date(data['date_start'])
        date_end = parse_str_to_date(data['date_end'])

        events = EventController.query_all_created_events_in_range(
            date_start, date_end)
        events_json = [{"n_events": 0} for i in range(12)]

        if events:
            for event in events:
                events_json[event.creation_date.month - 1]["n_events"] += 1

        return jsonify(events=events_json)
