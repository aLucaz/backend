from flask_restful import reqparse, Resource
from flask import json, jsonify
from application.resources.time_resources import parse_str_to_date
from application.controller.user_controller import UserController

parser = reqparse.RequestParser()
parser.add_argument('date_start', help='Not Blank', required=True)
parser.add_argument('date_end', help='Not Blank', required=True)


class AllCreatedUsers(Resource):
    def post(self):
        data = parser.parse_args()
        date_start = parse_str_to_date(data['date_start'])
        date_end = parse_str_to_date(data['date_end'])

        users = UserController.query_all_created_users_in_range(
            date_start, date_end)
        users_json = [{"n_users": 0} for i in range(12)]

        if users:
            for user in users:
                users_json[user.creation_date.month - 1]["n_users"] += 1

        return jsonify(users=users_json)
