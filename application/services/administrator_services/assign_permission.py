from flask_restful import Resource, reqparse
from application.models.user_model import UserModel
from flask import jsonify

from application.controller.person_controller import PersonController
from application.controller.user_controller import UserController
from application.controller.permission_controller import PermissionController

from application.resources.write_to_log import write_to_log

import json
from datetime import datetime

parser = reqparse.RequestParser()
parser.add_argument('users', help='Not Blank', required=True)
parser.add_argument('permission', help='Not Blank', required=True)

class AssignPermissions(Resource): 
      def post(self):
        data = parser.parse_args()
        
        json_users = json.loads(data['users'])
        for user in json_users: 
            if '@' in str(user['username']):
                current_person = PersonController.query_person_by_email(
                    user['username'])
                if not current_person:
                    return jsonify(status="error", message="Person with email {} not exist".format(user['username']))
                #se busca el usuario de la persona
                current_user = UserController.query_user_by_person_id(
                    current_person.id)
            else:
                current_user = UserController.query_user_by_username(
                    user['username'])
                if not current_user:
                    return jsonify(status="error", message='Username {} not exist'.format(user['username']))
            #una vez que se tiene la persona se le asigna el permiso
            permission = PermissionController.query_permission_by_name(data['permission'])
            if not permission: 
                return jsonify(status="error", message='Permission {} not exist'.format(data['permission']))
            #se agrega el permisio al usuario
            current_user.user_permission.append(permission)
            current_user.request_organizer = 0
            # ESTO NO TIENE SENTIDO PERO FUNCIONA :C
            UserController.insert_user(current_user)
            #UserController.commit_changes()
            write_to_log(datetime.now().strftime("%d-%m-%Y (%H:%M:%S): ") + "Se otorgo el permiso de " +
                         data['permission'] + " al usuario " + user['username'] + "\n", 'log.txt')


        return jsonify(status="ok", message='Permission assigned')


        
