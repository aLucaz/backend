from flask_restful import Resource, reqparse
from flask import jsonify, request
from application.resources.time_resources import clean_hour, parse_str_to_date
#todos las tablas que deben actualizar sus estados cuando cambia el tiempo 
from application.controller.event_controller import EventController 
from application.controller.phase_controller import PhaseController

parser = reqparse.RequestParser()
parser.add_argument('date', help='Not Blank', required=True)

class UpdateStates(Resource):
    def post(self): 
        data = parser.parse_args()
        # ya devuelve la fecha sin hora
        date_clean = parse_str_to_date(data['date'])
        # primero se actualizan todos los estados de los eventos 
        result = EventController.update_all_states_events(date_clean)
        if result.status == "error" : 
            return jsonify(status="error", message="error in events update")
        else: 
            result = PhaseController.update_all_states_phases(date_clean)
            if result.status == "error":
                return jsonify(status="error", message="error in phases update")
            else: 
                return jsonify(status="ok", message="estados actualizados")
            
