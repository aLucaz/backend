from flask_restful import Resource, reqparse
from flask import jsonify, json
from application.resources.time_resources import parse_str_to_date
from application.controller.time_controller import TimeController

parser = reqparse.RequestParser()
parser.add_argument('date', help='Not Blank', required=True)

class UpdateTime(Resource):
    def post(self):
        # recordar que viene como string
        data = parser.parse_args()
        date_clean = parse_str_to_date(data['date'])
        return TimeController.update_time(date_clean)
