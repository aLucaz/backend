from flask_restful import Resource, reqparse
from application.models.user_model import UserModel
from flask import jsonify

from application.controller.person_controller import PersonController
from application.controller.user_controller import UserController
from application.controller.permission_user_controller import PermissionUserController

class AllUsers(Resource):
    def post(self):
        users_json = []        
        for user, person in UserController.query_all_users():
            permissions = PermissionUserController.query_permissions_by_user_id(user.id) 
            #all permissions 
            all_permissions = []
            for permission in permissions:
                all_permissions.append({"id_permission": permission.id})
            
            users_json.append({
                "name": person.first_name, 
                "last_name": person.last_name,
                "username": user.username,  
                "email": person.email,
                "request_organizer": user.request_organizer,
                "permissions": all_permissions
            })
         
        del users_json[0]
        users_json.sort(key=lambda k: k['request_organizer'], reverse=True)

        return users_json
