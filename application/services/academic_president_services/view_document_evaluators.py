from flask_restful import Resource, reqparse
from application.controller.person_controller import PersonController
from application.controller.keyword_controller import KeywordController
from application.controller.submission_controller import SubmissionController
from application.controller.preference_controller import PreferenceController
from flask import jsonify, json

parser = reqparse.RequestParser()
parser.add_argument('submission_id', help='Not Blank', required=True)

class ViewDocumentEvaluators(Resource): 
	def post(self):
		data = parser.parse_args()
		evaluators = PersonController.query_evaluators_by_submission(submission_id=data['submission_id'])
		evaluators_json=[]
		for evaluator in evaluators:
			preference = PreferenceController.query_by_submission_and_user(submission_id=data['submission_id'],user_id=evaluator.UserModel.id)
			dicc = {
			"user_id":evaluator.UserModel.id,
			"first_name":evaluator.PersonModel.first_name,
			"last_name":evaluator.PersonModel.last_name,
			"preference":preference.level if preference else None,
			} 
			evaluators_json.append(dicc)

		return jsonify(evaluators=evaluators_json)