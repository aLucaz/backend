from flask_restful import Resource, reqparse
from application.controller.person_controller import PersonController
from application.controller.keyword_controller import KeywordController 
from application.controller.submission_controller import SubmissionController
from application.controller.preference_controller import PreferenceController
from flask import jsonify, json

parser = reqparse.RequestParser()
parser.add_argument('event_id', help='Not Blank', required=True)
parser.add_argument('phase_id', help='Not Blank', required=True)
parser.add_argument('submission_id', help='Not Blank', required=True)

class ViewEvaluatorsByEvent(Resource): 
	def post(self):
		data = parser.parse_args()
		evaluators = PersonController.\
			query_available_evaluators_by_event(event_id=data['event_id'],submission_id=data['submission_id'])
		#Primero se verificará que los autores del paper no puedan ser considerados como evaluadores
		authors = PersonController.query_authors_by_submission(submission_id=data['submission_id'])
		for author in authors:
			for evaluator in evaluators:
				if author.id == evaluator.PersonModel.id:
					evaluators.remove(evaluator)
					break
 
		evaluators_json=[]
		for evaluator in evaluators:
			preference = PreferenceController.\
				query_by_submission_and_user(submission_id=data['submission_id'],user_id=evaluator.UserModel.id)
			dicc = {
			"user_id":evaluator.UserModel.id,
			"first_name":evaluator.PersonModel.first_name,
			"last_name":evaluator.PersonModel.last_name,
			"preference":preference.level if preference else None,
			}
			evaluators_json.append(dicc)

		return jsonify(evaluators=evaluators_json)
