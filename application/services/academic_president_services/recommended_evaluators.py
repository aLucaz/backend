from flask_restful import Resource, reqparse
from application.controller.person_controller import PersonController 
from application.controller.preference_controller import PreferenceController 
from flask import jsonify, json

parser = reqparse.RequestParser()
parser.add_argument('event_id', help='Not Blank', required=True)
parser.add_argument('submission_id', help='Not Blank', required=True)

class RecommendedEvaluators(Resource): 
	def post(self):
		data = parser.parse_args()
		evaluators = PersonController.query_available_evaluators_by_event(event_id=data['event_id'],submission_id=data['submission_id'])
		#Primero se verificará que los autores del paper no puedan ser considerados como evaluadores
		authors = PersonController.query_authors_by_submission(submission_id=data['submission_id'])
		for author in authors:
			for evaluator in evaluators:
				if author.id == evaluator.PersonModel.id:
					evaluators.remove(evaluator)
					break

		listEvaluators=[[],[],[],[],[]]
		#listEvaluators[0] = 0- no asignó referencia (null)
		#listEvaluators[1] = 1- si me interesa
		#listEvaluators[2] = 2- quizas me interesa
		#listEvaluators[3] = 3- no me interesa
		#listEvaluators[4] = 4- conflicto de intereses
		for evaluator in evaluators:
			preference = PreferenceController.query_by_submission_and_user(submission_id=data['submission_id'], user_id=evaluator.UserModel.id)
			if not preference:
				level = 0
			else:
				level = preference.level
			listEvaluators[level].append(evaluator)

		assigned = []
		#El orden para asignar será 1-3-0-2-4 
		order = [1,2,0,3,4]
		for i in order:
			for evaluator in listEvaluators[i]:
				assigned.append((evaluator,i))
				if len(assigned)==5 : break
			if len(assigned)==5 : break

		evaluators_json = []
		for evaluator in assigned:
			dicc = {
			"user_id":evaluator[0].UserModel.id,
			"first_name":evaluator[0].PersonModel.first_name,
			"last_name":evaluator[0].PersonModel.last_name,
			"preference":evaluator[1]
			}
			evaluators_json.append(dicc)



		return jsonify(evaluators = evaluators_json)


