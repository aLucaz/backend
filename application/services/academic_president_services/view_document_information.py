from flask_restful import Resource, reqparse
from application.controller.submission_controller import SubmissionController
from application.controller.phase_controller import PhaseController
from application.controller.user_controller import UserController
from application.controller.person_controller import PersonController
from application.controller.keyword_controller import KeywordController

from flask import jsonify, json

parser = reqparse.RequestParser()
parser.add_argument('submission_id', help='Not Blank', required=True)

class ViewDocumentInformation(Resource): 
	def post(self):
		data = parser.parse_args()
		submission = SubmissionController.query_by_id(data['submission_id'])
		author_user = UserController.query_by_id(submission.user_id)
		author = PersonController.query_by_id(author_user.id)
		phase = PhaseController.query_phase_by_id(submission.phase_id)
		
		evaluators = UserController.query_evaluators_by_submission(submission.id)
		evaluator_json =[]
		for evaluator in evaluators:
			evaluator_json.append({"user_id":evaluator.UserModel.id})

		keywords = KeywordController.query_keywords_by_submission(submission.id)
		keywords_json = []
		for keyword in keywords:
			keywords_json.append({"name":keyword.name})
 
		return jsonify(name= submission.name,
					   summary=submission.summary,
					   author=author.first_name+" "+author.last_name,
					   phase=phase.name,
					   start_date=str(phase.start_date)[0:10],
					   end_date=str(phase.end_date)[0:10],
					   keywords=keywords_json,
					   evaluators=evaluator_json,
					   )
