from flask_restful import Resource, reqparse
from application.controller.user_controller import UserController 
from application.controller.submission_controller import SubmissionController
from application.controller.evaluation_controller import EvaluationController
from application.models.evaluation_model import EvaluationModel
from flask import jsonify, json
from application.resources.write_to_log import write_to_log
from datetime import datetime

parser = reqparse.RequestParser()
parser.add_argument('submission_id', help='Not Blank', required=True)
parser.add_argument('evaluators', help='Not Blank', required=True, action= "append")
parser.add_argument('username', help='Not Blank', required=False)

class AssignDocumentsBySubmission(Resource): 
	def post(self):
		data = parser.parse_args()
		submission = SubmissionController.query_by_id(data['submission_id'])
		now = datetime.now()
		user = UserController.query_user_by_username(data['username'])
		user_id = user.id if user else None
		for evaluator in data['evaluators']:
			evaluator_json =json.loads(evaluator.replace("'", "\""))
			user = UserController.query_by_id(evaluator_json['user_id'])
			evaluation = EvaluationController.query_evaluation_by_user_and_submission(user_id=user.id,submission_id = data['submission_id'])
			if not evaluation:
				evaluation = EvaluationModel(
					user = user,
					submission=submission,
					creation_date=now,
					creation_user_id=user_id
					)
			EvaluationController.insert_evaluation(evaluation)
			write_to_log(now.strftime("%d-%m-%Y (%H:%M:%S): ") + "Se asignó el entregable " +
			             str(data['submission_id']) + " al evaluador " + str(evaluator_json['user_id']) + "\n", 'log.txt')

		return jsonify(status= "ok", message="Documents assigned satisfactorily")


