from flask_restful import Resource, reqparse
from application.controller.phase_controller import PhaseController
from flask import jsonify, json

parser = reqparse.RequestParser()
parser.add_argument('event_id', help='Not Blank', required=True)

class GetPhaseByEvent(Resource): 
    def post(self):
        data = parser.parse_args()
        phase = PhaseController.query_by_id_event(data['event_id'])
        if not phase:
            return jsonify(phase_id=None)
        else:
            return jsonify(phase_id= phase.PhaseModel.id)