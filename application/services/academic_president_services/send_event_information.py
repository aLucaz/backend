from flask_restful import Resource, reqparse
from application.controller.event_controller import EventController
from application.controller.type_controller import TypeController 
from application.controller.keyword_controller import KeywordController 
from application.controller.person_controller import PersonController 
from flask import jsonify, json

parser = reqparse.RequestParser()
parser.add_argument('event_id', help='Not Blank', required=True)

class SendEventInformation(Resource): 
    def post(self):
        data = parser.parse_args()
        event =  EventController.query_event_by_id(data['event_id'])
        dicc = {
            "name":event.name,
            "description":event.description,
            "vacancies":event.remaining_vacancies,
            "location":event.location,
            "start_date":event.start_date.strftime("%d-%m-%Y"),
            "end_date":event.end_date.strftime("%d-%m-%Y"),
            "programme":event.programme,
            "picture":event.picture,
            "amount":event.amount
        }

        return jsonify(
            event=dicc
            )