from flask_restful import Resource, reqparse
from application.controller.event_controller import EventController
from application.controller.criterium_controller import CriteriumController
from application.controller.field_controller import FieldController
from application.controller.phase_controller import PhaseController

from flask import jsonify, json

parser = reqparse.RequestParser()
parser.add_argument('event_id', help='Not Blank', required=True)

class ViewEventPhasesInformation(Resource): 
	def post(self):
		data = parser.parse_args()
		event =  EventController.query_event_by_id(data['event_id'])
		phases_json=[]

		#obtiene todas las fases
		phases = PhaseController.query_all_phases(data['event_id'])
		print(phases)
		for phase in phases:
			fields_json=[]
			fields = FieldController.query_fields_by_id(phase.id)
			for field in fields:
				fields_json.append({"name":field.name})
			criteriums_json=[]
			criteriums = CriteriumController.query_by_phase_id(phase.id)
			for criterium in criteriums:
				criteriums_json.append({"name":criterium.name})

			dicc = { 
				"phase_id":phase.id,
				"name": phase.name,
				"status":phase.status,
				"description":phase.description,
				"start_date": phase.start_date.strftime("%Y-%m-%d"),
				"end_date": phase.end_date.strftime("%Y-%m-%d"),
				"needs_document":phase.needs_document,
				"fields":fields_json,
				"criteriums":criteriums_json,
				"limit_date":phase.limit_date.strftime("%Y-%m-%d") if phase.limit_date else None,
				"iscamrdy":phase.is_cam_rdy
			}

			phases_json.append(dicc)

		return jsonify(phases=phases_json)
