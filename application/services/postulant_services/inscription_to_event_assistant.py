from flask_restful import Resource, reqparse
from application.controller.person_controller import PersonController
from application.controller.user_controller import UserController
from application.controller.event_user_controller import EventUserController
from application.controller.event_controller import EventController
from application.controller.event_user_role_controller import EventUserRoleController
from application.controller.phase_controller import PhaseController
from application.controller.role_controller import RoleController
from application.controller.submission_controller import SubmissionController
from application.controller.keyword_controller import KeywordController
from application.controller.field_controller import FieldController
from application.resources.write_to_log import write_to_log
from application.models.event_user_model import EventUserModel
from application.models.person_model import PersonModel
from application.models.user_model import UserModel
from flask import jsonify, json
import datetime

parser = reqparse.RequestParser()
parser.add_argument('username', help='Not Blank', required=True)
parser.add_argument('password', help='Not Blank', required=True)
parser.add_argument('first_name', help='It can blank', required=False)
parser.add_argument('last_name', help='It can blank', required=False)
parser.add_argument('email', help='It can blank', required=False)
parser.add_argument('id_event', help='Not Blank', required=True)

class InscriptionToEventAssistant(Resource): 
    def post(self):
        data = parser.parse_args()
        current_user = UserController.query_user_by_username(
                data['username'])
        #current_person = PersonController.query_by_id(current_user.fk_person_id)
        if not current_user:
            current_person = PersonController.query_person_by_email(data['email'])
            if not current_person:
                #se debe crear la persona
                current_person = PersonModel(
                    first_name=data['first_name'],
                    last_name=data['last_name'],
                    email=data['email']
                )
                PersonController.insert_person(current_person)
            current_user = UserModel(
                username=data['username'],
                password=UserModel.generate_hash(data['password']),
                fk_person_id=current_person.id #No funcionó person=current_person
            )
            UserController.insert_user(current_user)

        event = EventController.query_event_by_id(data['id_event'])
        if event.remaining_vacancies > 0:
            event.remaining_vacancies = event.remaining_vacancies - 1
            EventController.insert_event(event)
        else:
            return jsonify(status="error", message="No se puede inscribir al evento al usuario {} debido a que ya no hay vacantes".format(data['username']))
        #primero se verifica que este ya creada la relacion event x user
        new_event_user = EventUserController.query_by_event_id_and_user_id(
            event_id=event.id, user_id=current_user.id)
        #si no está se crea
        if not new_event_user:
            new_event_user = EventUserModel(
                user=current_user,
                event=event
            )

        role = RoleController.query_role_by_name('Asistente')
        new_event_user.event_user_role.append(role)
        EventUserController.insert_event_user(new_event_user)
        
        write_to_log(datetime.datetime.now().strftime("%d-%m-%Y (%H:%M:%S): ") +
                     "Se registró al usuario " + data['username'] + " en el evento " + data['id_event'] + "\n", 'log.txt')

        return jsonify(status="ok", message="Persona con el usuario {} inscrita correctamente".format(data['username']))
