from flask_restful import Resource, reqparse
from application.controller.person_controller import PersonController
from application.controller.user_controller import UserController
from application.controller.event_user_controller import EventUserController
from application.controller.event_controller import EventController
from application.controller.event_user_role_controller import EventUserRoleController
from application.controller.phase_controller import PhaseController
from application.controller.field_controller import FieldController
from application.controller.keyword_controller import KeywordController
from application.controller.submission_controller import SubmissionController

from application.models.submission_model import SubmissionModel
from application.models.field_model import FieldModel
from application.models.keyword_model import KeywordModel
from application.models.person_model import PersonModel

from flask import jsonify
import json

from application.resources.aws_s3 import upload_file_to_s3, upload_image_to_s3, delete_file_of_s3

from werkzeug.datastructures import FileStorage
from werkzeug.utils import secure_filename

# HEIL FILESYSTEM
import os
from pathlib import Path
from datetime import datetime

#FILES MANAGE
application_folder = Path.cwd().joinpath('application')
papers_folder = application_folder.joinpath('files', 'papers')

ALLOWED_EXTENSIONS = ['pdf', 'jpg', 'png', 'jpeg', 'txt']

parser = reqparse.RequestParser()
parser.add_argument('id_submission', help='Not Blank', required=True)
parser.add_argument('id_phase', help='Not Blank', required=True)
parser.add_argument('username', help='Not Blank', required=True)
parser.add_argument('authors', help='Not Blank', required=False)
parser.add_argument('needs_document',type=int , help='Not Blank', required=True)
parser.add_argument('document', type=FileStorage,
                    help='Not Blank', required=False, location='files')
parser.add_argument('name', help='Not Blank', required=True)
parser.add_argument('summary', help='Not Blank', required=True)
parser.add_argument('keywords', help='Not Blank', required=False)
parser.add_argument('fields', help='Not Blank', required=False)

def allowed_file(filename):
    return '.' in filename and filename.rsplit('.', 1)[1].lower() in ALLOWED_EXTENSIONS

class UpdateSubmission(Resource): 
    def post(self):
        data = parser.parse_args()
        submission = SubmissionController.query_by_id(data['id_submission'])
        submission_id = submission.id
        now = datetime.now()
        user = UserController.query_user_by_username(data['username'])
        user_id =user.id

        # si se necesita subir archivos en la fase 
        file = None
        if data['needs_document'] == 1:
            file = data['document']	
        if file: 
            if file and allowed_file(file.filename):
                if submission.document: delete_file_of_s3(submission.document)
                file_secure_name = secure_filename(file.filename)
                file_url = upload_file_to_s3(file, file_secure_name)
            else:
                return jsonify(status="error", message="No se subió el paper")
        else:
            file_url= None

        phase = PhaseController.query_phase_by_id(data['id_phase'])

        if data['name']: submission.name=data['name'],
        if data['summary']: submission.summary=data['summary'],
        if data['document']: submission.document=file_url,
        if data['id_phase']: submission.phase=phase
        submission.last_modification_date=now
        submission.last_modified_user_id=user_id

        #primero vamos a actualizar los keywords
        kws = KeywordController.query_keywords_by_submission(submission_id)
        keywords = [kw.name for kw in kws]
        new = []

        json_keywords_data = json.loads(data['keywords'])
        print (json_keywords_data)
        for keyword in json_keywords_data:
            if not keyword in keywords:
                new.append(keyword)
            else:
                keywords.remove(keyword)

        #lo que está en new se agrega a la relación
        for keyword in new:
            new_keyword = KeywordController.query_keyword_by_name(keyword)
            if not new_keyword:
                new_keyword = KeywordModel(name=keyword,creation_date=now,creation_user_id=user_id)
                notify = KeywordController.insert_keyword(new_keyword)
                if notify.status == "error":
                    return jsonify(status="error", message="Something wrong in Keyword Registration")
            # se agrega en la tabla intermedia
            new_keyword.submission_keywords.append(submission)
            KeywordController.insert_keyword(new_keyword)
        #lo restante en keywords se elimina, ya que quiere decir que ya no estan en las seleccionadas por el usuario
        for keyword in keywords:
            keyword_model = KeywordController.query_keyword_by_name(keyword)
            #se elimina de la tabla intermedia
            keyword_model.submission_keywords.remove(submission)
            KeywordController.insert_keyword(keyword_model)

        #ahora se actualizan los autores del paper
        authors = PersonController.query_authors_by_submission(submission_id)
        delete = [{"email":a.email} for a in authors]
        new = []

        json_authors_data = json.loads(data['authors'])
        for author in json_authors_data:
            encontrado = False
            for a in delete:
                if author['email']==a["email"]:
                    delete.remove(a)
                    encontrado=True
                    person_aux = PersonController.query_person_by_email(author['email'])
                    person_aux.affiliation = author['affiliation']
                    PersonController.insert_person(person_aux)
                    break
            #no lo encontró
            if not encontrado: new.append(author)

        #se insertan los nuevos authores registrados
        for author in new:
            person_aux = PersonController.query_person_by_email(author['email'])
            if not person_aux:
                person_aux = PersonModel(
                    first_name=author['name'],
                    last_name=author['last_name'],
                    email=author['email'],
                    affiliation=author['affiliation'],
                    creation_date=now,
                    creation_user_id=user_id
                )
                notify = PersonController.insert_person(person_aux)
                if notify.status == "error":
                    return jsonify(status="error", message="Something wrong in Person-Submission Registration")
            person_aux.submission_persons.append(submission)
            PersonController.insert_person(person_aux)

        #finalmente se elimina la relación de los que ya no estan
        for author in delete:
            person_aux = PersonController.query_person_by_email(
                author['email'])
            person_aux.submission_persons.remove(submission)
            PersonController.insert_person(person_aux)

        #se mantiene al que subio el paper como autor
        #current_user = UserController.query_user_by_username(data['username'])
        #current_person =PersonController.query_person_by_user_id(current_user.id)
        #current_person.submission_persons.append(submission)
        #PersonController.insert_person(current_person)
        
        #ahora se actualizan los campos en la base de datos        
        json_fields_data = json.loads(data['fields'])
        for field in json_fields_data: 
            notify = FieldController.update_content(field['id_field'], field['content_field'])
            if notify.status == "error":
                return jsonify(status="error", message="Something wrong in Field Registration")

        #se actualizan los cambios en la BD
        notify = SubmissionController.insert_submission(submission)
        if notify.status == "error":
            return jsonify(status="error", message="Something wrong in Event Update")

        return jsonify(status="ok", message="Submission updated satisfactorily")
