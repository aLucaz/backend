from flask_restful import Resource, reqparse
from application.controller.phase_controller import PhaseController
from application.controller.submission_controller import SubmissionController
from application.controller.keyword_controller import KeywordController
from application.controller.user_controller import UserController
from application.controller.preference_controller import PreferenceController
from application.controller.person_controller import PersonController


from flask import jsonify, json


parser = reqparse.RequestParser()
parser.add_argument('username', help='Not Blank', required=True)
parser.add_argument('phase_id', help='Not Blank', required=True)

class GetSubmissions(Resource): 
    def post(self):
        data = parser.parse_args()
        if '@' in str(data['username']):
            current_person = PersonController.query_person_by_email(data['username'])
            if not current_person:
                return jsonify(status="error", message="Person with email {} not exist".format(data['username']))
            #se busca el usuario de la persona
            current_user = UserController.query_user_by_person_id(current_person.id)
        else:
            current_user = UserController.query_user_by_username(data['username'])
            if not current_user:
                return jsonify(status="error", message='Username {} not exist'.format(data['username']))

        current_phase = PhaseController.query_phase_by_id(data['phase_id'])
        if not current_phase:
            return jsonify(status="error", message="Phase doesn't exist")

        submissions = SubmissionController.query_submission_by_phase(
            current_phase.id)

        submission_json = []
        for submission in submissions:
            preference = PreferenceController.query_by_submission_and_user(submission.id, current_user.id)
            keywords_json = []
            keywords = KeywordController.query_keywords_by_submission(submission.id)
            for keyword in keywords:
                keywords_json.append(keyword.name)
            username = UserController.query_by_id(submission.user_id).username
            authors_json = []
            authors = PersonController.query_authors_by_submission(submission.id)
            for author in authors:
                authors_json.append(author.first_name+" "+author.last_name)
            if not preference:
                submission_json.append({
                    "id": submission.id,
                    "name": submission.name,
                    "user": username,
                    "category": keywords_json,
                    "authors": authors_json})

        return jsonify(submisions=submission_json)
