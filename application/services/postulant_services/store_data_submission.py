from flask_restful import Resource, reqparse
from application.controller.person_controller import PersonController
from application.controller.user_controller import UserController
from application.controller.event_user_controller import EventUserController
from application.controller.event_controller import EventController
from application.controller.event_user_role_controller import EventUserRoleController
from application.controller.phase_controller import PhaseController
from application.controller.field_controller import FieldController
from application.controller.keyword_controller import KeywordController
from application.controller.submission_controller import SubmissionController
from application.resources.write_to_log import write_to_log
from application.models.submission_model import SubmissionModel
from application.models.field_model import FieldModel
from application.models.person_model import PersonModel

from flask import jsonify
import json
from application.resources.aws_s3 import upload_file_to_s3, upload_image_to_s3, delete_file_of_s3

from werkzeug.datastructures import FileStorage
from werkzeug.utils import secure_filename

# HEIL FILESYSTEM
import os
from pathlib import Path
from datetime import datetime


#FILES MANAGE
application_folder = Path.cwd().joinpath('application')
papers_folder = application_folder.joinpath('files', 'papers')

ALLOWED_EXTENSIONS = ['pdf', 'jpg', 'png', 'jpeg', 'txt', 'zip']

parser = reqparse.RequestParser()
parser.add_argument('id_event', help='Not Blank', required=True)
parser.add_argument('id_phase', help='Not Blank', required=True)
parser.add_argument('username', help='Not Blank', required=True)
parser.add_argument('authors', help='Not Blank', required=True)
parser.add_argument('needs_document',type=int , help='Not Blank', required=True)
parser.add_argument('document', type=FileStorage,
                    help='Not Blank', required=False, location='files')
parser.add_argument('name', help='Not Blank', required=True)
parser.add_argument('summary', help='Not Blank', required=True)
parser.add_argument('keywords', help='Not Blank', required=True)
parser.add_argument('fields', help='Not Blank', required=True)

def process_date(date):
    try: 
        int(date[0])
        return datetime.strptime((date.replace("T", " "))[0:19], '%Y-%m-%d %H:%M:%S')
    except ValueError:
        return datetime.strptime(date[0:25], '%a, %d %b %Y %H:%M:%S')


def allowed_file(filename):
    return '.' in filename and filename.rsplit('.', 1)[1].lower() in ALLOWED_EXTENSIONS

class StoreDataSubmission(Resource): 
    def post(self):
        data = parser.parse_args()
        now = datetime.now()
        #id del evento  
        id_event = data['id_event']
        # se verifica existencia del usuario
        if '@' in str(data['username']):
            current_person = PersonController.query_person_by_email(
                data['username'])
            if not current_person:
                return jsonify(status="error", message="Person with email {} not exist".format(data['username']))
            #se busca el usuario de la persona
            current_user = UserController.query_user_by_person_id(current_person.id)
        else:
            current_user = UserController.query_user_by_username(data['username'])
            if not current_user:
                return jsonify(status="error", message='Username {} not exist'.format(data['username']))
        
        user_id =current_user.id
        id_phase = data['id_phase']
        event = EventController.query_event_by_id(id_event)
        phase = PhaseController.query_phase_by_id(id_phase)
        
        # si se necesita subir archivos en la fase        
        file = None
        if data['needs_document'] == 1:
            file = data['document']	
        if file: 
            if file and allowed_file(file.filename):
                file_secure_name = secure_filename(file.filename)
                file_url = upload_file_to_s3(file, file_secure_name)
            else:
                return jsonify(status="error", message="No se subió el paper")
        else:
            file_url= None
        
        new_submission = SubmissionModel(
            name=data['name'],
            summary=data['summary'],
            document=file_url,
            user=current_user,
            phase=phase,
            creation_date=now,
            creation_user_id=user_id
        )

        #extraer keyword del submission
        json_keywords_data = json.loads(data['keywords'])
        for key in json_keywords_data:
            key_aux = KeywordController.query_keyword_by_name(key)
            if not key_aux:
                return jsonify(status="error", message="Something wrong in Keyword Controller")
            key_aux.submission_keywords.append(new_submission)         
        
        #ahora se insertan los autores del paper
        json_authors_data = json.loads(data['authors'])
        for author in json_authors_data:
            person_aux = PersonController.query_person_by_email(author['email'])
            if not person_aux:
                person_aux = PersonModel(
                    first_name=author['name'],
                    last_name=author['last_name'],
                    email=author['email'],
                    affiliation=author['affiliation'],
                    creation_date=now,
                    creation_user_id=user_id,
                )
                notify = PersonController.insert_person(person_aux)
                if notify.status == "error":
                    return jsonify(status="error", message="Something wrong in Person-Submission Registration")
            person_aux.submission_persons.append(new_submission)

        #Se agrega al que sube el paper como author
        #current_person =PersonController.query_person_by_user_id(current_user.id)
        #current_person.submission_persons.append(new_submission)
        
        notify = SubmissionController.insert_submission(new_submission)
        if notify.status == "error":
            return jsonify(status="error", message="Something wrong in Event Registration")
  
        #ahora se agregan los campos en la base de datos        
        json_fields_data = json.loads(data['fields'])
        for field in json_fields_data: 
            notify = FieldController.update_content(field['id_field'], field['content_field'])
            if notify.status == "error":
                return jsonify(status="error", message="Something wrong in Field Registration")

        write_to_log(datetime.now().strftime("%d-%m-%Y (%H:%M:%S): ") + "El usuario " +
                     data['username'] + " insertó el submission \"" + data['name'] + "\"\n", 'log.txt')

        return jsonify(status="ok", message="Submission {} created satisfactorily".format(data['name']))
