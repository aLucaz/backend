from flask_restful import Api
from flask_sqlalchemy import SQLAlchemy, event
from flask_cors import CORS
from flask_script import Manager
from flask_migrate import Migrate, MigrateCommand
from flask import Flask
# API CONFIG
app = Flask(__name__)
api = Api(app)
CORS(app)

# DATABASE CONFIGURATION 
from application.resources.database import create_session_server
db = create_session_server()

# SERVER CONFIGURATION
from application.models.user_model import UserModel
from application.models.type_model import TypeModel
from application.models.person_model import PersonModel
from application.models.permission_model import PermissionModel
from application.models.keyword_model import KeywordModel
from application.models.event_model import EventModel
from application.models.event_user_model import EventUserModel
from application.models.role_model import RoleModel
from application.models.criterium_model import CriteriumModel
from application.models.evaluation_model import EvaluationModel
from application.models.submission_model import SubmissionModel
from application.models.preference_model import PreferenceModel
from application.models.time_model import TimeModel

### AGREGAR NUEVA INFORMACIÓN  A LA BASE DE DATOS
migrate = Migrate(app, db)
manager = Manager(app)
manager.add_command('db', MigrateCommand)

from application.controller.type_controller import TypeController
from application.controller.role_controller import RoleController
from application.controller.permission_controller import PermissionController
from application.controller.administrator_controller import AdministratorController
from application.controller.time_controller import TimeController
from application.resources.time_resources import get_today

@app.before_first_request
def create_default_values():
    TypeController.insert_all_types()
    RoleController.insert_all_roles()
    PermissionController.insert_all_permissions()
    AdministratorController.insert_administrator()    
    TimeController.insert_time(get_today())

# SERVICIOS GENERALES
from application.services.general_services import login
from application.services.general_services import google_login
from application.services.general_services import registration
from application.services.general_services import google_registration
from application.services.general_services import all_keywords
from application.services.general_services import all_roles
from application.services.general_services import all_types
from application.services.general_services import all_permissions
from application.services.general_services import check_administrator_permission
from application.services.general_services import check_organizer_permission
from application.services.general_services import my_events
from application.services.general_services import update_data_user
from application.services.general_services import update_password_user
from application.services.general_services import send_user_data
from application.services.general_services import count_submissions_by_phase
from application.services.general_services import verify_existence
from application.services.general_services import send_programme
from application.services.general_services import send_email_person
from application.services.general_services import delete_file
from application.services.general_services import verify_account
from application.services.general_services import get_previous_submission_keywords

# SERVICIOS DE ADMINISTRADOR
from application.services.administrator_services import all_users
from application.services.administrator_services import send_notification
from application.services.administrator_services import assign_permission
from application.services.administrator_services import all_activated_events
from application.services.administrator_services import all_activated_users
from application.services.administrator_services import all_created_events
from application.services.administrator_services import all_created_users
from application.services.administrator_services import send_time
from application.services.administrator_services import update_time 
from application.services.administrator_services import update_states



# SERVICIOS DE ORGANIZADOR
from application.services.organizer_services import create_event
from application.services.organizer_services import send_invitations
from application.services.organizer_services import update_event_phases
from application.services.organizer_services import update_event
from application.services.organizer_services import all_phases_event
from application.services.organizer_services import upload_phase_data
from application.services.organizer_services import request_organizer_permission
from application.services.organizer_services import view_event_information
from application.services.organizer_services import specific_event_roles
from application.services.organizer_services import get_phase_date


# SERVICIOS DE EVALUADOR
from application.services.evaluator_services import evaluate_papers
from application.services.evaluator_services import select_preferences
from application.services.evaluator_services import evaluation_services 
from application.services.evaluator_services import send_submission
from application.services.evaluator_services import evaluation_info
from application.services.evaluator_services import view_data_evaluation_evaluator
from application.services.evaluator_services import send_evaluation_data


# SERVICIOS DE PRESIDENTE DE COMITE ACADÉMICO
from application.services.academic_president_services import assign_documents_by_evaluator
from application.services.academic_president_services import assign_documents_by_submission
from application.services.academic_president_services import get_phase_by_event
from application.services.academic_president_services import view_document_evaluators
from application.services.academic_president_services import view_document_information
from application.services.academic_president_services import view_evaluators_by_event
from application.services.academic_president_services import view_submissions
from application.services.academic_president_services import view_evaluations_by_submission
from application.services.academic_president_services import register_president_evaluation
from application.services.academic_president_services import send_evaluation_results
from application.services.academic_president_services import send_invitations_inscription
from application.services.academic_president_services import recommended_evaluators 
from application.services.academic_president_services import view_data_evaluation_president
from application.services.academic_president_services import view_event_phases_information
from application.services.academic_president_services import send_event_information

# SERVICIOS DE POSTULANTE
from application.services.postulant_services import inscription_to_event_postulant
from application.services.postulant_services import inscription_to_event_assistant
from application.services.postulant_services import send_submission_data
from application.services.postulant_services import submission_services
from application.services.postulant_services import store_data_submission
from application.services.postulant_services import update_submission


# API RESOURCES 
api.add_resource(login.Login, '/login')
api.add_resource(google_login.GoogleLogin, '/google_login')
api.add_resource(registration.Registration, '/registration')
api.add_resource(google_registration.GoogleRegistration, '/google_registration')
api.add_resource(all_keywords.AllKeywords, '/allkeywords')
api.add_resource(all_roles.AllRoles, '/allroles')
api.add_resource(all_types.AllTypes, '/alltypes')
api.add_resource(all_permissions.AllPermissions, '/allpermissions')
api.add_resource(verify_existence.VerifyExistence, '/verify_existence')
api.add_resource(check_organizer_permission.CheckOrganizer, '/check_organizer')
api.add_resource(check_administrator_permission.CheckAdministrator, '/check_administrator_permission')
api.add_resource(send_time.SendTime, '/send_time')
api.add_resource(update_states.UpdateStates, '/update_states')
api.add_resource(update_time.UpdateTime, '/update_time')
api.add_resource(all_activated_events.AllActivatedEvents, '/all_activated_events')
api.add_resource(all_created_events.AllCreatedEvents, '/all_created_events')
api.add_resource(all_created_users.AllCreatedUsers, '/all_created_users')
api.add_resource(all_activated_users.AllActivatedSessions, '/all_activated_users')
api.add_resource(delete_file.DeleteFile, '/delete_file')
api.add_resource(create_event.CreateEvent, '/create_event')
api.add_resource(send_invitations.SendInvitations, '/send_invitations')
api.add_resource(assign_permission.AssignPermissions, '/assign_permission')
api.add_resource(all_users.AllUsers, '/all_users')
api.add_resource(all_phases_event.AllPhasesEvent, '/all_phases')
api.add_resource(my_events.MyEvents, '/my_events')
api.add_resource(specific_event_roles.SpecificEventRoles, '/specific_event_roles')
api.add_resource(upload_phase_data.UploadPhaseData, '/upload_phase_data')
api.add_resource(store_data_submission.StoreDataSubmission, '/store_data_submission')
api.add_resource(send_user_data.SendUserData, '/send_user_data')
api.add_resource(verify_account.VerifyAccount, '/verify_account')
api.add_resource(view_data_evaluation_evaluator.ViewDataEvaluationEvaluator, '/view_data_evaluation_evaluator')
api.add_resource(view_data_evaluation_president.ViewDataEvaluationPresident, '/view_data_evaluation_president')
api.add_resource(request_organizer_permission.RequestOrganizerPermission, '/request_organizer_permission')
api.add_resource(send_email_person.SendEmailPerson, '/send_email_person')
api.add_resource(send_submission_data.SendSubmissionData, '/send_submission_data')
api.add_resource(send_evaluation_data.SendEvaluationData, '/send_evaluation_data')
api.add_resource(inscription_to_event_postulant.InscriptionToEventPostulant, '/inscription_to_event_postulant')
api.add_resource(inscription_to_event_assistant.InscriptionToEventAssistant, '/inscription_to_event_assistant')
api.add_resource(send_event_information.SendEventInformation, '/send_event_information')
api.add_resource(send_invitations_inscription.SendInvitationsInscription, '/send_invitations_inscription')
api.add_resource(update_data_user.UpdateDataUser, '/update_data_user')
api.add_resource(update_password_user.UpdatePasswordUser, '/update_password_user')
api.add_resource(view_evaluators_by_event.ViewEvaluatorsByEvent, '/view_evaluators_by_event')
api.add_resource(assign_documents_by_submission.AssignDocumentsBySubmission, '/assign_documents_by_submission')
api.add_resource(assign_documents_by_evaluator.AssignDocumentsByEvaluator, '/assign_documents_by_evaluator')
api.add_resource(view_document_information.ViewDocumentInformation, '/view_document_information')
api.add_resource(count_submissions_by_phase.CountSubmissionsByPhase, '/count_submissions_by_phase')
api.add_resource(view_document_evaluators.ViewDocumentEvaluators, '/view_document_evaluators')
api.add_resource(get_phase_by_event.GetPhaseByEvent, '/get_phase_by_event')
api.add_resource(send_notification.SendNotification, '/send_notifications')
api.add_resource(send_programme.SendProgramme, '/send_programme')
api.add_resource(submission_services.GetSubmissions, '/submission_services')
api.add_resource(select_preferences.InsertPreferences, '/select_preferences')
api.add_resource(get_phase_date.GetPhaseDate, '/get_phase_date')
api.add_resource(evaluation_info.GetEvaluationInfo, '/evaluation_info') 
api.add_resource(view_submissions.ViewSubmissions, '/view_submissions')
api.add_resource(evaluate_papers.UpdateEvaluation, '/evaluate_papers')
api.add_resource(recommended_evaluators.RecommendedEvaluators, '/recommended_evaluators')
api.add_resource(view_event_information.ViewEventInformation, '/view_event_information')
api.add_resource(view_event_phases_information.ViewEventPhasesInformation, '/view_event_phases_information')
api.add_resource(update_event.UpdateEvent, '/update_event')
api.add_resource(update_event_phases.UpdateEventPhases, '/update_event_phases')
api.add_resource(view_evaluations_by_submission.ViewEvaluationsBySubmission, '/view_evaluations_by_submission')
api.add_resource(register_president_evaluation.RegisterPresidentEvaluation, '/register_president_evaluation')
api.add_resource(send_evaluation_results.SendEvaluationResults, '/send_evaluation_results')
api.add_resource(get_previous_submission_keywords.GetPreviousSubmissionKeywords, '/get_previous_submission_keywords')
api.add_resource(send_submission.SendSubmission, '/send_submission')
api.add_resource(update_submission.UpdateSubmission, '/update_submission')
api.add_resource(evaluation_services.MyEvaluations, '/evaluation_services')













