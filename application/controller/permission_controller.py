#from application.api import db
from application.resources.database import db_session
from application.models.permission_model import PermissionModel
from flask import jsonify
from sqlalchemy.orm.exc import NoResultFound


class PermissionController():
    
    @classmethod
    def insert_permission(cls, permission):
        session = db_session()
        try:
            session.add(permission)
            session.commit()
            return jsonify(status="ok", message="permission {} inserted".format(permission.name))
        except:
            session.rollback()
            return jsonify(status="error", message="error in insert_permission")
        finally:
            session.close()

    @classmethod
    def query_permission_by_name(cls, name): 
        session = db_session()
        try:
            return session.query(PermissionModel).filter_by(name=name).first() 
        except NoResultFound:
            return None
        finally:
            session.close()

    @classmethod
    def query_all_permissions(cls):
        
        def to_json(x):
            return {
                "name": x.name
            }
        session = db_session()
        try:
            return jsonify(types=list(map(lambda x: to_json(x), session.query(PermissionModel).all())))
        except NoResultFound:
            return None
        finally:
            session.close()

    @classmethod
    def insert_all_permissions(cls):
        if not cls.query_permission_by_name("Administrator"):
            cls.insert_permission(PermissionModel(name="Administrator"))
        
        if not cls.query_permission_by_name("Organizer"):
            cls.insert_permission(PermissionModel(name="Organizer"))
