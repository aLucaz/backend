from application.resources.database import db_session
from application.models.time_model import TimeModel
from flask import jsonify
from sqlalchemy.orm.exc import NoResultFound


class TimeController():
    @classmethod
    def insert_time(cls, date):
        session = db_session()
        aux_time = session.query(TimeModel).first()
        try:
            if aux_time: 
                aux_time.time = date 
                session.commit()
            else: 
                time = TimeModel(time= date)
                session.add(time)
                session.commit()

            return jsonify(status="ok", message="time inserted")
        except:
            session.rollback()
            return jsonify(status="error", message="error in insert_time")
        finally:
            session.close()
    
    @classmethod 
    def query_time(cls): 
        session = db_session()
        try:
            return session.query(TimeModel).first()            
        except:
            session.rollback()
            return None
        finally:
            session.close()

    @classmethod
    def update_time(cls, new_date):
        session = db_session()
        try:
            time = session.query(TimeModel).first()
            time.time = new_date 
            session.commit()
        except:
            session.rollback()
            return None
        finally:
            session.close()
