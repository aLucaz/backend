from application.resources.database import db_session
from application.models.criterium_model import CriteriumModel
from application.models.evaluation_model import EvaluationModel
from application.models.criterium_evaluation_model import CriteriumEvaluationModel
from flask import jsonify
from sqlalchemy.orm.exc import NoResultFound

class CriteriumEvaluationController():
    @classmethod
    def insert(cls, criterium_evaluation):
        session = db_session()
        try:
            session.add(criterium_evaluation)
            session.commit()
            return jsonify(status="ok", message="success")
        except:
            session.rollback()
            return jsonify(status="error", message="error in insert_criterium_evaluation")
        finally:
            session.close()

    @classmethod
    def query_by_evaluation(cls, evaluation_id):
        session = db_session()
        try:
            return session.query(CriteriumEvaluationModel, CriteriumModel).\
                    filter(CriteriumEvaluationModel.evaluation_id == evaluation_id).\
                    filter(CriteriumEvaluationModel.criterium_id == CriteriumModel.id).all()
        except NoResultFound:
            return None
        finally:
            session.expunge_all()
            session.close()

    @classmethod
    def query_by_evaluation_by_criterium(cls, evaluation_id, criterium_id):
        session = db_session()
        try:
            return session.query(CriteriumEvaluationModel).filter_by(evaluation_id=evaluation_id).filter_by(criterium_id=criterium_id).first()
        except NoResultFound:
            return None
        finally:
            session.close()