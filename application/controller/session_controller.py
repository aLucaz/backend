from application.resources.database import db_session
from flask import jsonify
from sqlalchemy.orm.exc import NoResultFound
from datetime import datetime
from application.models.session_model import SessionModel

class SessionController():

    @classmethod
    def insert_session(cls, user_session):
        session = db_session()
        try:
            session.add(user_session)
            session.commit()
            return jsonify(status="ok", message="user_session inserted")
        except:
            session.rollback()
            return jsonify(status="error", message="error in insert_user")
        finally:
            session.close()

    @classmethod
    def query_all_sessions_in_range(cls, start_date, end_date):
        session = db_session()
        try:
            return session.query(SessionModel).\
                filter(start_date <= SessionModel.date_session).\
                filter(end_date >= SessionModel.date_session).\
                all()

        except NoResultFound:
            return None
        finally:
            session.close()
