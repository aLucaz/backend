from application.resources.database import db_session
from application.models.user_model import UserModel 
from application.models.person_model import PersonModel
from application.controller.user_controller import UserController
from application.controller.person_controller import PersonController
from application.controller.permission_controller import PermissionController
from flask import jsonify
from sqlalchemy.orm.exc import NoResultFound


class AdministratorController():
    @classmethod
    def insert_administrator(cls):
        session = db_session()
        try:
            new_person = PersonModel(
                    first_name="admin",
                    last_name="admin",
                    email="admin@rendezvous.com"
            )
            new_user = UserModel(
                username="admin",
                password=UserModel.generate_hash("admin"), 
                person=new_person
            )
            permission = PermissionController.query_permission_by_name("Administrator")
            if not permission:
                return jsonify(status="error", message='Permission {} not exist'.format("Administrator"))
            new_user.user_permission.append(permission)
            PersonController.insert_person(new_person)
            UserController.insert_user(new_user)

            return jsonify(status="ok", message="administrator inserted")
        except:
            session.rollback()
            return jsonify(status="error", message="error in insert_administrator")
        finally:
            session.close()
