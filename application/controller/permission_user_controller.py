from application.resources.database import db_session
from application.models.user_model import UserModel
from application.models.permission_model import PermissionModel
from application.models.permission_model import user_permission as PermissionUserModel
from flask import jsonify
from sqlalchemy.orm.exc import NoResultFound


class PermissionUserController():
    @classmethod
    def query_permissions_by_user_id(cls, user_id):
        session = db_session()
        try:
            return session.query(PermissionModel).\
                filter(UserModel.id == user_id).\
                filter(PermissionUserModel.c.user_id == UserModel.id).\
                filter(PermissionUserModel.c.permission_id == PermissionModel.id).all()
        except NoResultFound:
            return None
        finally:
            session.close()
