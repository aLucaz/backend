from application.resources.database import db_session
from application.models.evaluation_model import EvaluationModel
from application.models.submission_model import SubmissionModel
from application.models.user_model import UserModel
from application.models.person_model import PersonModel
from flask import jsonify
from sqlalchemy.orm.exc import NoResultFound


class EvaluationController():

    @classmethod
    def query_by_id(cls, id_eval):
        session = db_session()
        try:
            return session.query(EvaluationModel).\
                filter(EvaluationModel.id == id_eval).first()
        except NoResultFound:
            return None
        finally:
            session.expunge_all()
            session.close()

    @classmethod
    def query_evaluation_by_user_and_phase(cls, user_id, phase_id):
        session = db_session()
        try:
            return session.query(EvaluationModel, SubmissionModel).\
                    join(SubmissionModel).\
                    filter(SubmissionModel.phase_id == phase_id).\
                    filter(EvaluationModel.user_id == user_id).all()
        except NoResultFound:
            return None
        finally:
            session.expunge_all()
            session.close()

    @classmethod
    def query_evaluations_by_submission(cls, submission_id):
        session = db_session()
        try:
            return session.query(EvaluationModel,PersonModel).\
                    filter(EvaluationModel.submission_id == submission_id).\
                    filter(EvaluationModel.user_id == UserModel.id).\
                    filter(UserModel.fk_person_id == PersonModel.id).\
                    all()
        except NoResultFound:
            return None
        finally:
            session.expunge_all()
            session.close()

    @classmethod
    def query_evaluation_by_user_and_submission(cls, user_id, submission_id):
        session = db_session()
        try:
            return session.query(EvaluationModel).filter(EvaluationModel.user_id==user_id).filter(EvaluationModel.submission_id==submission_id).first()
        except NoResultFound:
            return None
        finally:
            session.close()

    @classmethod
    def insert_evaluation(cls, evaluation):
        session = db_session()
        try:
            session.add(evaluation)
            session.commit()
            return jsonify(status="ok", message="evaluation inserted")
        except:
            session.rollback()
            return jsonify(status="error", message="error in insert_keyword")
        finally:
            session.close()

