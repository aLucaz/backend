from application.resources.database import db_session
from application.models.submission_model import SubmissionModel
from application.models.evaluation_model import EvaluationModel
from application.models.phase_model import PhaseModel
from application.models.user_model import UserModel
from application.controller.user_controller import UserController
from application.controller.event_user_controller import EventUserController
from flask import jsonify
from sqlalchemy.orm.exc import NoResultFound
from datetime import datetime

class SubmissionController():

    @classmethod
    def insert_submission(cls, submission):
        session = db_session()
        try:
            session.add(submission)
            session.commit()
            return jsonify(status="ok", message="Submission {} inserted".format(submission.name))
        except:
            session.rollback()
            return jsonify(status="error", message="error in insert_user")
        finally:
            session.close()

    @classmethod
    def query_by_phase(cls, phase_id):
        session = db_session()
        try:
            return session.query(SubmissionModel, UserModel).join(UserModel).filter(SubmissionModel.phase_id == phase_id).filter(SubmissionModel.user_id == UserModel.id)
        except NoResultFound:
            return None
        finally:
            session.close()

    @classmethod
    def query_by_user_and_phase(cls, user_id, phase_id):
        session = db_session()
        try:
            return session.query(SubmissionModel).filter_by(user_id=user_id).filter_by(phase_id=phase_id).first()
        except NoResultFound:
            return None
        finally:
            session.close

    @classmethod
    def query_by_evaluator(cls, user_id):
        session = db_session()
        try:
            return session.query(SubmissionModel, EvaluationModel).filter(SubmissionModel.id == EvaluationModel.submission_id).filter(EvaluationModel.user_id == user_id).all()
        except NoResultFound:
            return None
        finally:
            session.close

    @classmethod
    def query_by_id(cls, id):
        session = db_session()
        try:
            return session.query(SubmissionModel).filter_by(id=id).first()
        except NoResultFound:
            return None
        finally:
            session.close()

    # XD
    @classmethod
    def count_submissions_by_phase(cls, phase_id):
        session = db_session()
        try:
            return session.query(SubmissionModel).filter_by(phase_id=phase_id).count()
        except NoResultFound:
            return None
        finally:
            session.close()

    @classmethod
    def query_submission_by_phase(cls, phase_id):
        session = db_session()
        try:
            return session.query(SubmissionModel).filter_by(phase_id=phase_id).all()
        except NoResultFound:
            return None
        finally:
            session.close()

    @classmethod
    def query_submission_by_name(cls, submission_name):
        session = db_session()
        try:
            return session.query(SubmissionModel).filter_by(name=submission_name).first()
        except NoResultFound:
            return None
        finally:
            session.close()

    @classmethod
    def query_submission_by_name_and_phase(cls, submission_name, phase_id):
        session = db_session()
        try:
            return session.query(SubmissionModel).filter_by(phase_id=phase_id).filter_by(name=submission_name).first()
        except NoResultFound:
            return None
        finally:
            session.close()

    @classmethod
    def update_submission_result(cls, id, result, comment, username):
        session = db_session()
        try:
            submission = session.query(SubmissionModel).filter_by(id=id).first()
            submission.president_result = result 
            submission.president_comment = comment
	        
            now = datetime.now()
            user = UserController.query_user_by_username(username)
            user_id = user.id if user else None
            
            submission.last_modification_date = now
            submission.last_modified_user_id = user_id

            if int(result) == 0: 
                event_user = EventUserController.query_events_by_user_id(submission.user_id) 
                event_user.status = 0
            
            session.commit()
            return jsonify(status="ok", message="field updated")
        except NoResultFound:
            return jsonify(status="error", message="field no encontrado ")
        finally:
            session.close()
