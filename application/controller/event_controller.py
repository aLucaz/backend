#from application.api import db
from application.resources.database import db_session
from application.models.event_model import EventModel
from flask import jsonify
from sqlalchemy.orm.exc import NoResultFound

class EventController():
    @classmethod
    def insert_event(cls, event):
        session = db_session()
        try:
            session.add(event)
            session.commit()
            return jsonify(status="ok", message="event {} inserted".format(event.name))
        except:
            session.rollback()
            return jsonify(status="error", message="error in insert_event")
        finally:
            session.close()
    #suave con este
    @classmethod
    def flush_event(cls, new_event): 
        session = db_session()
        session.add(new_event)
        session.flush()
        session.close()

    @classmethod
    def query_event_by_name(cls, name):
        session = db_session()
        try:
            return session.query(EventModel).filter_by(name=name).first()
        except NoResultFound:
            return None
        finally:
            session.close()
			
    @classmethod
    def query_event_by_id(cls, id):
        session = db_session()
        try:
            return session.query(EventModel).filter(EventModel.id==id).first()
        except NoResultFound:
            return None
        finally:
            session.close()
	
    @classmethod
    def query_all_events(cls):

        def to_json(x):
            return {
                'name': x.name,
                'description': x.description,
                'vacancies': x.vacancies,
                'location': x.location,
                'description': x.description,
                'programme': x.programme,
                'picture': x.picture,
                'start_date': x.start_date,
                'end_date': x.end_date
            }
        session = db_session()
        try:
            return jsonify(events=list(map(lambda x: to_json(x), session.query(EventModel).all())))
        except NoResultFound:
            return None
        finally:
            session.close()

    @classmethod 
    def update_all_states_events(cls, date): 
        session = db_session()
        events = session.query(EventModel).all()
        try:
            if events: 
                for event in events: 
                    if date >= event.start_date and date <= event.end_date: 
                        event.status = 1
                    elif date < event.start_date or date > event.end_date:
                        event.status = 0
                    
                    session.commit()
                return jsonify(status="ok")
            else:   
                return jsonify(status="error", message="there aren't events")
        finally: 
            session.close()

    @classmethod 
    def query_all_events_in_range(cls, start_date, end_date): 
        session = db_session() 
        try:
            return session.query(EventModel).\
                    filter(start_date <= EventModel.start_date).\
                    filter(end_date >= EventModel.start_date).\
                    filter(EventModel.status == 1).\
                    all()
            
        except NoResultFound:
            return None
        finally: 
            session.close()
    
    @classmethod
    def query_all_created_events_in_range(cls, start_date, end_date):
        session = db_session()
        try:
            return session.query(EventModel).\
                filter(start_date <= EventModel.creation_date).\
                filter(end_date >= EventModel.creation_date).\
                all()

        except NoResultFound:
            return None
        finally:
            session.close()
