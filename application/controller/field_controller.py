from application.resources.database import db_session
from application.models.field_model import FieldModel
from flask import jsonify
from sqlalchemy.orm.exc import NoResultFound
from sqlalchemy import update

class FieldController():
    @classmethod
    def insert_field(cls, field):
        session = db_session()
        try:
            session.add(field)
            session.commit()
            return jsonify(status="ok", message="field {} inserted".format(field.name))
        except:
            session.rollback()
            return jsonify(status="error", message="error in insert_field")
        finally:
            session.close()

    @classmethod
    def delete_field(cls, field):
        session = db_session()
        try:
            session.delete(field)
            session.commit()
            return jsonify(status="ok", message="field {} deleted".format(field.name))
        except:
            session.rollback()
            return jsonify(status="error", message="error in delete_field")
        finally:
            session.close()

    @classmethod
    def query_by_id(cls, id):
        session = db_session()
        try:
            return session.query(FieldModel).filter(FieldModel.id==id).first()
        except NoResultFound:
            return None
        finally:
            session.close()

    @classmethod
    def query_fields_by_id(cls, id):
        session = db_session()
        try:
            return session.query(FieldModel).filter(FieldModel.phase_id==id).all()
        except NoResultFound:
            return None
        finally:
            session.close()
    
    @classmethod 
    def update_content(cls, id, new_content):
        session = db_session()
        try:
            my_field = session.query(FieldModel).filter(FieldModel.id == id).first()
            my_field.content = new_content 
            session.commit()
            return jsonify(status="ok", message="field updated")
        except NoResultFound:
            return jsonify(status="error", message="field no encontrado ")
        finally:
            session.close()
