from application.resources.database import db_session
from application.models.phase_model import PhaseModel
from application.models.event_model import EventModel
from flask import jsonify
from sqlalchemy.orm.exc import NoResultFound


class PhaseController():
    @classmethod
    def insert_phase(cls, phase):
        session = db_session()
        try:
            session.add(phase)
            session.commit()
            return jsonify(status="ok", message="phase {} inserted".format(phase.name))
        except:
            session.rollback()
            return jsonify(status="error", message="error in insert_phase")
        finally:
            session.close()

    @classmethod
    def delete_phase(cls, phase):
        session = db_session()
        try:
            session.delete(phase)
            session.commit()
            return jsonify(status="ok", message="phase {} deleted".format(phase.name))
        except:
            session.rollback()
            return jsonify(status="error", message="error in delete_phase")
        finally:
            session.close()

    @classmethod
    def query_phase_by_id(cls, phase_id):
        session = db_session()
        try:
            return session.query(PhaseModel).filter_by(id=phase_id).first()
        except NoResultFound:
            return None
        finally:
            session.close()

    @classmethod
    def query_by_id_event(cls, id_event):
        session = db_session()
        try:
            return session.query(EventModel, PhaseModel).\
                filter(EventModel.id == PhaseModel.event_id).\
                filter(EventModel.id==id_event).\
                filter(PhaseModel.status==1).first()
        except NoResultFound:
            return None
        finally:
            session.close()
		
    @classmethod
    def query_all_phases(cls, id_event):
        session = db_session()
        try:
            return session.query(PhaseModel).filter(PhaseModel.event_id==id_event).all()
        except NoResultFound:
            return None
        finally:
            session.close()

    @classmethod
    def activatePhase(cls, id_phase):
        session = db_session()
        try:
            phase = session.query(PhaseModel).filter(PhaseModel.id == id_phase).first()
            phase.status = 1
            session.commit() 
            return jsonify(status="ok", message="phase activated")
        except NoResultFound:
             return jsonify(status="error", message="phase not found")
        finally:
            session.close()

    @classmethod 
    def deactivatePhase(cls, id_phase):
        session = db_session()
        try:
            phase = session.query(PhaseModel).filter(PhaseModel.id == id_phase).first()
            phase.status = 0
            session.commit()
            return jsonify(status="ok", message="phase deactivated")
        except NoResultFound:
             return jsonify(status="error", message="phase not found")
        finally:
            session.close()

    @classmethod 
    def update_all_states_phases(cls, date):
        session = db_session()
        phases = session.query(PhaseModel).all()
        if phases: 
            for phase in phases: 
                if date >= phase.start_date and date <= phase.end_date: 
                    phase.status = 1
                elif date < phase.start_date or date > phase.end_date:
                    phase.status = 0
                
                session.commit()
            return jsonify(status="ok")
        else:   
            return jsonify(status="error", message="there aren't phases")

        session.close()
