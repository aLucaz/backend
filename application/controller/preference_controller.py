#from application.api import db
from application.resources.database import db_session
from application.models.preference_model import PreferenceModel
from flask import jsonify
from sqlalchemy.orm.exc import NoResultFound

class PreferenceController():
    @classmethod 
    def insert_preference(cls, preference):
        session = db_session()
        try:
            session.add(preference)
            session.commit()
            return jsonify(status="ok", message="preference {} inserted".format(preference.name))
        except:
            session.rollback()
            return jsonify(status="error", message="error in insert_preference")
        finally:
            session.close()

    @classmethod
    def query_by_submission_and_user(cls, submission_id, user_id):
        session = db_session()
        try:
            return session.query(PreferenceModel).filter_by(submission_id=submission_id).filter_by(user_id=user_id).first()
        except NoResultFound:
            return None
        finally:
            session.close()

            